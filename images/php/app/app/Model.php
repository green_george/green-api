<?php

namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Traits\MappedAttributes;
use App\Traits\EncryptedAttributes;
use App\Scopes\FMStoreScope;

abstract class Model extends EloquentModel
{
    use MappedAttributes, EncryptedAttributes {
        MappedAttributes::setAttribute as setAttributeMA;
        EncryptedAttributes::setAttribute as setAttributeEA;
    }

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'wm_dashboard';

    /**
     * Override in child classes if the Model has no "store_id" field
     * (Avoids adding the global FMStoreScope)
     */
    const NO_STORE_ID = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'store_id' ];

    /**
     * Set a given attribute on the model.
     * (Just here to handle conflicting method definitions from AttributeMap and EncryptedAttributes traits)
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        $this->errorIfOriginalAttribute($key, $value);
        return $this->setAttributeEA($key, $value);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        if (static::NO_STORE_ID === true)
            static::addGlobalScope(new FMStoreScope);
    }

    /**
     * Ensures the pivot
     *
     * @param Illuminate\Database\Eloquent\Relations\Relation $relation
     * @param int $storeId
     */
    protected static function storeIdOnPivot(Relation $relation, int $storeId = 9)
    {
        return $relation->wherePivot('store_id', $storeId);
    }
}
