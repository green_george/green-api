<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Override attribute names using the $mappedAttributes array.
 */
trait MappedAttributes
{
    /**
     * The attributes that should be renamed.
     *
     * @var array
     */
    protected $mappedAttributes = [];
    private $unmappedAttributes = [];

    /**
     * Initialize this trait (initialize[TraitName] methods are called automatically by Illuminate's Model class)
     */
    public function initializeMappedAttributes()
    {
        $this->unmappedAttributes = array_flip($this->mappedAttributes);
    }

    // Model init

    /**
     * Set the array of model attributes. No checking is done.
     *
     * @param  array  $attributes
     * @param  bool  $sync
     * @return $this
     */
    public function setRawAttributes(array $attributes, $sync = false)
    {
        // Remap the attribute keys
        $mappedAttributes = $this->getMappedAttributes($attributes);

        // Continue with normal logic
        return parent::setRawAttributes($mappedAttributes, $sync);
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        $mappedAttributes = $this->getMappedAttributes($attributes);
        return parent::fill($mappedAttributes);
    }

    /**
     * Set a given attribute on the model.
     * NOTE: Overridden to throw an Exception when the original attribute name is used.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        $this->errorIfOriginalAttribute($key, $value);
        parent::setAttribute($key, $value);
    }

    /**
     * Throw an Exception when the original attribute name is used.
     *
     * @param  string  $key
     * @param  mixed  $value
     */
    protected function errorIfOriginalAttribute($key, $value)
    {
        $mappedKey = $this->mappedAttributes[$key] ?? null;
        if ($mappedKey !== null) throw new \Exception("AttributeMap: \"$key\" was mapped to \"$mappedKey\" - use that instead.");
    }

    // Saving

    /**
     * Perform a model update operation.
     * NOTE: Logic copied from parent and slightly modified. Changes marked by NOTE comments.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return bool
     */
    protected function performUpdate(Builder $query)
    {
        // If the updating event returns false, we will cancel the update operation so
        // developers can hook Validation systems into their models and cancel this
        // operation if the model does not pass validation. Otherwise, we update.
        if ($this->fireModelEvent('updating') === false) {
            return false;
        }

        // First we need to create a fresh query instance and touch the creation and
        // update timestamp on the model which are maintained by us for developer
        // convenience. Then we will just continue saving the model instances.
        if ($this->usesTimestamps()) {
            $this->updateTimestamps();
        }

        // Once we have run the update operation, we will fire the "updated" event for
        // this model instance. This will allow developers to hook into these after
        // models are updated, giving them a chance to do any special processing.
        $dirty = $this->getMappedAttributes($this->getDirty(), true); // NOTE: Modified from original function

        if (count($dirty) > 0) {
            $this->setKeysForSaveQuery($query)->update($dirty);

            $this->syncChanges();

            $this->fireModelEvent('updated', false);
        }

        return true;
    }

    /**
     * Perform a model insert operation.
     * NOTE: Logic copied from parent and slightly modified. Changes marked by NOTE comments.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return bool
     */
    protected function performInsert(Builder $query)
    {
        if ($this->fireModelEvent('creating') === false) {
            return false;
        }

        // First we'll need to create a fresh query instance and touch the creation and
        // update timestamps on this model, which are maintained by us for developer
        // convenience. After, we will just continue saving these model instances.
        if ($this->usesTimestamps()) {
            $this->updateTimestamps();
        }

        // If the model has an incrementing key, we can use the "insertGetId" method on
        // the query builder, which will give us back the final inserted ID for this
        // table from the database. Not all tables have to be incrementing though.
        $attributes = $this->getMappedAttributes(null, true); // NOTE: Modified from original function

        if ($this->getIncrementing()) {
            $this->insertAndSetId($query, $attributes);
        }

        // If the table isn't incrementing we'll simply insert these attributes as they
        // are. These attribute arrays must contain an "id" column previously placed
        // there by the developer as the manually determined key for these models.
        else {
            if (empty($attributes)) {
                return true;
            }

            $query->insert($attributes);
        }

        // We will go ahead and set the exists property to true, so that it is set when
        // the created event is fired, just in case the developer tries to update it
        // during the event. This will allow them to do so and run an update here.
        $this->exists = true;

        $this->wasRecentlyCreated = true;

        $this->fireModelEvent('created', false);

        return true;
    }

    // Private functions

    /**
     * Get attributes with mapped keys.
     *
     * @param  array|null  $attributes
     * @param bool $unmap
     * @return array
     */
    private function getMappedAttributes($attributes = null, $unmap = false)
    {
        $attributes = $attributes ?: $this->getAttributes();
        if (count($this->mappedAttributes) === 0) return $attributes;
        $mapKeyFunc = $unmap ? 'getUnmappedKey' : 'getMappedKey';
        return collect($attributes)->mapWithKeys(function($item, $key) use ($mapKeyFunc) {
            $modifiedKey = $this->$mapKeyFunc($key);
            return [ $modifiedKey => $item ];
        })->all();
    }

    /**
     * Get the updated key - after using mappings, if any.
     *
     * @param string $key
     * @return string
     */
    private function getMappedKey(string $key)
    {
        return $this->mappedAttributes[$key] ?? $key;
    }

    /**
     * Get the original key - before using mappings, if any.
     *
     * @param string $key
     * @return string
     */
    private function getUnmappedKey(string $key)
    {
        return $this->unmappedAttributes[$key] ?? $key;
    }
}
