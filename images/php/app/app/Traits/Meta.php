<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use App\Relations\HasMeta;

/**
 * To configure, use $this->initMeta(...); in __construct()
 */
trait Meta
{
    private $metaTable;
    private $metaForeignKey;

    /**
     * Initialize any required properties.
     *
     * @param string $metaTable
     * @param string $metaForeignKey - optional, defaults to "[snake case class name]_id"
     */
    private function initMeta($metaTable, $metaForeignKey = null, $casts = [])
    {
        $this->metaTable = $metaTable;
        $this->metaForeignKey = $metaForeignKey;
        $this->casts = $casts;
    }

    public function getMetaTable()
    {
        return $this->metaTable;
    }

    /**
     * Get the foreign key for the meta table.
     *
     * @return string
     */
    public function getMetaForeignKey()
    {
        return $this->metaForeignKey ?: snake_case(class_basename($this)) . '_id';
    }

    /**
     * Begin querying a model with eager loading.
     * (Overridden to convert meta column SELECT's into WHERE clauses)
     *
     * @param  array|string  $relations
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function with($relations)
    {
        $metas = [];

        // Strip out any meta keys (keeping track of the columns in $meta)
        $strippedRelations = array_filter(
            is_string($relations) ? func_get_args() : $relations,
            function($relation) use (&$metas) {
                $data = explode('meta:', $relation);
                if (count($data) < 2) return true;
                $metas += explode(',', $data[1]);
                return false;
            }
        );

        // If we have meta select "columns", replace them with a whereIn
        if (count($metas) > 0) {
            $strippedRelations = array_merge($strippedRelations, [
                'meta' => function ($query) use ($metas) {
                    $query->whereIn('meta_key', $metas);
                },
            ]);
        }

        return (new static)->newQuery()->with($strippedRelations);
    }

    /**
     * Set up the meta data relation.
     */
    public function meta()
    {
        $fk = $this->getMetaForeignKey();
        return new HasMeta($this->metaTable, $this, $fk, 'id', $this->casts);
    }

    /**
     * Narrow query results to models where the provided meta information exists.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $metaKey - optional
     * @param string|array $metaValue - optional
     * @return @param \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMetaExists($query, $metaKey = null, $metaValue = [])
    {
        $tableName = $this->table;
        $metaTableName = $this->metaTable;
        $fk = $this->getMetaForeignKey();
        $metaValues = (array) $metaValue;
        return $query->whereExists(function ($query) use ($tableName, $metaTableName, $fk, $metaKey, $metaValues) {
            $query->select(DB::raw(0))->from($metaTableName)->whereColumn($fk, "$tableName.id");
            if ($metaKey !== null) $query->where('meta_key', $metaKey);
            if (count($metaValues) === 0) return;
            $query->whereIn('meta_value', $metaValues);
        });
    }
}
