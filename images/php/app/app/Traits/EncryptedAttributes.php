<?php

namespace App\Traits;

/**
 * Makes automatic encryption/decryption possible through the $encryptedAttributes array.
 */
trait EncryptedAttributes
{
    /**
     * The attributes that should be decrypted/encrypted.
     *
     * @var array
     */
    protected $encryptedAttributes = [];

    /**
     * Convert the model's attributes to an array.
     *
     * @return array
     */
    public function attributesToArray()
    {
        $array = parent::attributesToArray();

        $array = collect($array)->mapWithKeys(function($value, $key) {
            $value = in_array($key, $this->encryptedAttributes)
                ? $this->decrypt($value, $key)
                : $value;
            return [ $key => $value ];
        })->all();

        return $array;
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        return $value && in_array($key, $this->encryptedAttributes)
            ? $this->decrypt($value, $key)
            : $value;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        return parent::setAttribute($key, in_array($key, $this->encryptedAttributes) ? $this->encrypt($value) : $value);
    }

    /**
     * Decrypt the field encrypted in Green Applications fashion
     */
    protected function decrypt($value, $attributeName = null)
    {
        // NOTE: Wrapped in a try/catch because some fields weren't always encrypted (so half the data still isn't encrypted)
        try {
            $salt = config('constants.SALT');
            $cipher = config('constants.CIPHER');
            $key = substr(strtoupper(md5($salt)), 0, 16);
            $iv = substr(strtoupper(md5(strrev($salt))), 0, 16);
            $decrypted = openssl_decrypt(hex2bin($value), $cipher, $key, null, $iv);
            $decrypted = mb_convert_encoding($decrypted, 'UTF-8', 'UTF-8'); // Protect against invalid data [like wm_addresses WHERE id IN (691, 692, 693)]
            return rtrim($decrypted, "\0"); // Trim the whitespace from the end.
        } catch (\Exception $e) {

            // Set the attribute so it will be saved if ->save() is called (encrypted this time)
            if ($attributeName !== null)
                $this->setAttribute($attributeName, $this->encrypt($value));

            return $value;
        }
    }

    /**
     * Encrypt the field encrypted in Green Applications fashion
     */
    protected function encrypt($decrypted)
    {
        $salt = config('constants.SALT');
        $cipher = config('constants.CIPHER');

        $key = substr(strtoupper(md5($salt)), 0, 16);
        $iv = substr(strtoupper(md5(strrev($salt))), 0, 16);
        $encrypted = openssl_encrypt(rtrim($decrypted, "\0"), $cipher, $key, null, $iv);
        return bin2hex($encrypted);
    }
}
