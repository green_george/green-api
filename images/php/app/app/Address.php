<?php

namespace App;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use net\authorize\api\constants\ANetEnvironment;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    const BILLING_ADDRESS_TYPE = 1;
    const SHIPPING_ADDRESS_TYPE = 2;

    const TERM_MAP = [
        'Cash on Delivery' => 'COD',
        'Credit Card' => 'CC',
        'Net 1 Payment Terms' => 'NET1',
        'Net 5 Payment Terms' => 'NET5',
        'Net 7 Payment Terms' => 'NET7',
        'Net 10 Payment Terms' => 'NET10',
        'Net 15 Payment Terms' => 'NET15',
        'Net 30 Payment Terms' => 'NET30',
        'Net 45 Payment Terms' => 'NET45',
        'Net 60 Payment Terms' => 'NET60',
        'Net 90 Payment Terms' => 'NET90',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_addresses';

    /**
     * The attributes that aren't mass assignable.
     * @see https://laravel.com/docs/5.7/eloquent#mass-assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be decrypted/encrypted.
     *
     * @var array
     */
    protected $encryptedAttributes = [
        'shipping_name',
        'first_name',
        'last_name',
        'street1',
        'street2',
        'street3',
        'street3',
        'city',
        'region',
        'zipcode',
        'country',
        'phone_number',
        'fax_number',
        'email_address',
    ];

    /**
     * The attributes that should be renamed.
     * (remove "address_" prefix)
     *
     * @var array
     */
    protected $mappedAttributes = [
        'address_type' => 'type',
        'address_cim' => 'cim',
        'address_mis' => 'mis',
    ];

    /**
     * Build and return an Address model representing the shipping department.
     *
     * @return \App\Address
     */
    public static function getShippingDepartmentAddress()
    {
        return new self([
            'shipping_name' => 'Shipping Department',
            'company_name' => 'Green Applications',
            'street1' => '401 Taylor Ave',
            'city' => 'Gordonsville',
            'region' => 'VA',
            'zipcode' => '22942',
            'country' => 'US',
        ]);
    }

    /**
     * Get the shipping name.
     *
     * @param string|null $value
     * @return string
     */
    public function getShippingNameAttribute($value)
    {
        return $value ?? $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get the payment method type.
     *
     * @return string
     */
    public function getPaymentMethodTypeAttribute()
    {
        // Determined based on ->paymentMethodDescription
        return static::TERM_MAP[$this->paymentMethodDescription ?? ''] ?? '';
    }

    /**
     * Get the payment method description.
     *
     * @return string
     */
    public function getPaymentMethodDescriptionAttribute()
    {
        if ($this->cim > 0) return 'Credit Card';

        /*
            if a billing address has cim <= 0, then we should assume it is not a credit card and instead some user terms.
            'to find the user_term_desc (i.e. the payment method description),
            query the wm_user_terms table where user_term_type = 1 (billing terms) and address_id = the id of the billing address.
            If there is a result, the user_term_desc will correspond to the payment method description.
            This is a written out full description, map to make smaller below...
        */
        $userTerm = DB::table('wm_user_terms')
            ->where('user_term_type', 1)
            ->where('address_id', $this->id)
            ->first();

        $desc = $userTerm->user_term_desc ?? null;
        return $desc === 'COD' ? 'Cash on Delivery' : $desc; // COD mapping is reversed in the data
    }

    /**
     * Build an array using this address that fits the format accepted by Shippo.
     * @see https://goshippo.com/docs/reference#addresses
     *
     * @return array
     */
    public function toShippoFormatArray()
    {
        return [
            'name' => $this->shipping_name,
            'company' => $this->company_name,
            'street1' => $this->street1,
            'street2' => $this->street2,
            'city' => $this->city,
            'state' => $this->region,
            'zip' => $this->zipcode,
            'country' => $this->country,
        ];
    }

    /**
     * Gets credit card information associated with this address from AuthorizeNet.
     *
     * @return array - [ 'creditCardType' => ?, 'creditCardLast4Digits' => ? ]
     */
    public function getAuthorizeNetCreditCardDetails()
    {
        try {
            // Get ID's from models
            $customerProfileId = $this->customer->cim;
            $customerPaymentProfileId = $this->cim;

            // Set up API details to request against
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName(config('constants.MERCHANT_LOGIN_ID'));
            $merchantAuthentication->setTransactionKey(config('constants.MERCHANT_TRANSACTION_KEY'));

            // Create API request with appropriate information
            $request = new AnetAPI\GetCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setCustomerProfileId($customerProfileId);
            $request->setCustomerPaymentProfileId($customerPaymentProfileId);

            // Get response using the API controller
            $controller = new AnetController\GetCustomerPaymentProfileController($request);
            $response = $controller->executeWithApiResponse(ANetEnvironment::PRODUCTION);

            // Handle possible errors
            if ($response === null) throw new \Exception('Bad response');
            $responseMessages = $response->getMessages();
            if ($responseMessages->getResultCode() !== 'Ok') {
                throw new \Exception(implode("\n",
                    array_map(function($errorMessage) {
                        return $errorMessage->getCode() . ' - ' . $errorMessage->getText();
                    }, $responseMessages->getMessage())
                ));
            }

            // Return credit card info
            $cc = $response->getPaymentProfile()->getPayment()->getCreditCard();
            return [
                'creditCardType' => $cc->getCardType(),
                'creditCardLast4Digits' => $cc->getCardNumber(),
            ];
        } catch(\Exception $e) {
            return [
                'creditCardType' => null,
                'creditCardLast4Digits' => null,
            ];
        }
    }

    /**
     * Get the Customer model related to this Address.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'user_id');
    }
}
