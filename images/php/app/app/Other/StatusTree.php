<?php

namespace App\Other;

class StatusTree
{
    // Level 1 Grouping
    const CLOSED = 'Closed';
    const OPEN = 'Open';

    // Level 2 Grouping
    const FULFILLED = 'Fulfilled';
    const SHIPPED = 'Shipped';
    const CANCELLED = 'Cancelled';
    const IN_PRODUCTION = 'In Production';
    const PREFLIGHT = 'Pre-flight';
    const PAYMENT_DECLINED = 'Payment Declined';

    // Level 3 Grouping
    const STATUS_MAP = [
        -1 => 'UNKNOWN',
        1 => 'UNFULFILLED',
        2 => 'PARTIALLY UNFULFILLED',
        3 => 'FULFILLED',
        4 => 'OVER FULFILLED',
        5 => 'ON BACKORDER',
        6 => 'SHORT-SHIPPED, FULFILLED',
        7 => 'CANCELLED',
        8 => 'IMPORT FAILED',
        9 => 'PREFLIGHT IN PROGRESS',
        10 => 'PREFLIGHT STALLED',
        11 => 'PREFLIGHT COMPLETE',
        12 => 'PREFLIGHT ON HOLD',
        13 => 'PREFLIGHT VERIFY',
        14 => 'ORDER ON HOLD',
        15 => 'EXPORT FAILED',
        16 => 'EXPORT LIMBO',
        17 => 'PREPRESS',
        30 => 'OVER ALLOCATED',
        31 => 'AT WMS',
        32 => 'WMS EXPORT FAILED',
        33 => 'READY TO SHIP',
        34 => 'READY TO STOCK',
        40 => 'REQUIRES REVIEW',
        41 => 'REQUIRES REP APPROVAL',
        42 => 'REQUIRES ADMIN APPRIVAL',
        43 => 'REQUIRES ART APPROVAL',
        50 => 'UNPAID',
        70 => 'UNPRODUCED',
        71 => 'IN PRODUCTION',
        75 => 'UNFINISHED',
        76 => 'IN FINISHING',
        50 => 'UNPAID',
        51 => 'FULFILLED + UNPAID',
        52 => 'MISSING IMAGES',
        80 => 'QUOTE ORDER',
        81 => 'INVOICE',
    ];

    const TREE_NODES = [
        self::CLOSED => [
            self::FULFILLED => [ 3 ],
            self::CANCELLED => [ 7 ],
        ],
        self::OPEN => [
            self::SHIPPED => [ 3 ],
            self::IN_PRODUCTION => [ 1, 14, 15, 17, 30, 31, 32, 33, 71 ],
            self::PREFLIGHT => [ 9, 10, 11, 12, 13, 39 ],
            self::PAYMENT_DECLINED => [ 50 ],
        ],
    ];

    public function getMatchingNode($keyToMatch, $parentNode = null)
    {
        $parentNode = $parentNode ?? static::TREE_NODES;
        foreach ($parentNode as $key => $childNode) {
            // Skip leaf nodes
            if (is_numeric($key)) return null;

            // Check this node
            if ($key === $keyToMatch) return $childNode;

            // Check node's children
            if (is_array($childNode)) {
                $match = $this->getMatchingNode($keyToMatch, $childNode);
                if ($match !== null) return $match;
            }
        }
        return null;
    }

    public function getLeafNodes($parentNode)
    {
        if (!is_array($parentNode)) return [ $parentNode ];
        return array_flatten($parentNode);
    }

    public function getStatusesForGroup($group)
    {
        $node = $this->getMatchingNode($group);
        return $node !== null ? $this->getLeafNodes($node) : [];
    }

    public function getGroupForStatus(int $status, $level = 1)
    {
        // Get an array of group names
        $groups = $this->getGroupsForStatus($status);

        // Ensure $index is in the bounds of the array
        $lastIndex = count($groups) - 1;
        $index = $level - 1;
        $index = max($index, 0);
        $index = min($index, $lastIndex);

        // Return the group at $level - 1
        return $groups[$index];
    }

    public function getGroupsForStatus(int $status)
    {
        // Flatten to single-level array and flip to get a dictionary array like [ $statusNumber => $dotNotationKeys, ... ]
        $flipped = array_flip(array_dot(static::TREE_NODES));

        // Convert $dotNotationKeys into an array
        $newArray = [];
        foreach ($flipped as $key => $value) {
            $newArray[$key] = array_reverse(
                array_slice(explode('.', $value), 0, -1) // remove the last value, since it's just a number
            );
        }

        // Try to return a match from the dictionary
        return $newArray[$status] ?? [ 'UNKNOWN' ];
    }
}
