<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class DiscountCollection extends Collection
{
    /**
     * Apply multiple discounts to a single value.
     *
     * @param float $value - The value to be discounted.
     * @param float $totalPrice - Total base price at the given quantity.
     * @param boolean $unitMode - When true, $value is considered a unit price. Otherwise, it is a total price.
     * @param int $quantity - The actual quantity of products.
     * @param int $lookupQuantity - Used instead of $quantity for pricing table lookups.
     * @return float - The discounted value.
     */
    public function applyTo($value, $totalPrice, $unitMode = false, $quantity = 1, $lookupQuantity = -1)
    {
        // Return early if there's nothing to process
        if ($this->count() === 0) return $value;

        $lookupQuantity = $lookupQuantity === -1 ? $quantity : $lookupQuantity;
        $multiplier = $unitMode === true ? 1 : $quantity;

        // Filter out any invalid discounts
        $validDiscounts = $this->getValidDiscounts($totalPrice, $quantity);

        // Calculate discount amount and subtract it from the given value
        return $value - $validDiscounts->calculateDiscount($totalPrice, $lookupQuantity, $multiplier);
    }

    /**
     * Limit the discounts to those that are valid.
     *
     * @param float $totalPrice - Total base price at the given quantity.
     * @param int $quantity - The actual quantity of products.
     * @return App\DiscountCollection
     */
    private function getValidDiscounts($totalPrice, $quantity)
    {
        return $this->filter(function ($discount) use ($totalPrice, $quantity) {
            return !(
                ($discount->discount_min_price > 0 && $totalPrice < $discount->discount_min_price) ||
                ($discount->discount_max_price > 0 && $totalPrice > $discount->discount_min_price) ||
                ($discount->discount_min_quantity > 0 && $quantity < $discount->discount_min_quantity) ||
                ($discount->discount_max_quantity > 0 && $quantity > $discount->discount_max_quantity) ||
                ($discount->discount_expiration > 0 && $discount->discount_expiration < time()) ||
                ($discount->discount_uses === 0) ||
                ($discount->discount_behavior !== 2 && $discount->discount_code !== '-1')
            );
        });
    }

    /**
     * Calculate the different discount total types and return one of them.
     *
     * @param float $totalPrice - Total base price at the given quantity.
     * @param int $lookupQuantity - Used instead of $quantity for pricing table lookups.
     * @param int $multiplier - The actual quantity of products || 1 (for unit price calculation)
     * @return float - The selected discount total.
     */
    private function calculateDiscount($totalPrice, $lookupQuantity, $multiplier)
    {
        // Split discounts into one of three types - priceList, stackable, or unstackable
        $dictionary = $this->mapToDictionary(function ($discount) {
            $isPriceList = $discount->discount_behavior === 2; // 0 is "units off", 1 is unknown and unused
            $key = $isPriceList ? 'priceList' : ( $discount->is_stackable ? 'stackable': 'unstackable' );
            return [ $key => $discount ];
        });

        // Use the priceListDiscount, if any
        $priceListDiscount = $this->calculatePriceListDiscount($dictionary->get('priceList', []), $lookupQuantity, $multiplier);
        if ($priceListDiscount > 0) return $priceListDiscount;

        // Otherwise, continue processing and return the greater discount between $stackableDiscount and $unstackableDiscount
        $stackableDiscount = $this->calculateNonPriceListDiscounts($dictionary->get('stackable', []), $totalPrice);
        $unstackableDiscount = $this->calculateNonPriceListDiscounts($dictionary->get('unstackable', []), $totalPrice);
        return max($stackableDiscount, $unstackableDiscount);
    }

    /**
     * Calculates pricelist discounts.
     *
     * @param array $discounts - Discounts to sum up.
     * @param int $lookupQuantity - Used instead of $quantity for pricing table lookups.
     * @param int $multiplier - The actual quantity of products || 1 (for unit price calculation)
     * @return float - The discount total.
     */
    private function calculatePriceListDiscount($discounts, $lookupQuantity, $multiplier)
    {
        return array_reduce($discounts, function ($runningTotal, $discount) use ($lookupQuantity, $multiplier) {
            // If this discount can't match up to a pricingTable, ignore this discount
            if (!is_numeric( $discount->discount_code ) || $discount->discount_code <= 0)
                return $runningTotal;

            // If there's no valid pricing table, ignore this discount
            $pricingTable = PricingTable::find($discount->discount_code);
            if (!isset($tier)) return $runningTotal;

            // If there's no valid tier, ignore this discount
            $tier = $pricingTable->findTier($lookupQuantity);
            if (!isset($tier)) return $runningTotal;

            // Update the running total
            if ($pricingTable->mode === 'PRECISE')
                return $runningTotal + ($tier->price * $multiplier);
            else if ($pricingTable->mode === 'ONETIME')
                return $runningTotal + $tier->price;

            // Ignore if this is unsupported
            else
                return $runningTotal;
        }, 0.00);
    }

    /**
     * Calculates non-pricelist discounts.
     *
     * @param array $discounts - Discounts to sum up.
     * @param float $totalPrice - Total base price at the given quantity. Used for "percent off" discounts
     * @return float - The discount total.
     */
    private function calculateNonPriceListDiscounts($discounts, $totalPrice)
    {
        return array_reduce($discounts, function ($runningTotal, $discount) use ($totalPrice) {
            // Only process "percent off" discounts ("dollar off" discounts are unused)
            if ($discount->discount_unit !== 0) return $runningTotal;
            return $runningTotal + ($totalPrice * (( 100 - $discount->discount_amount ) / 100));
        }, 0.00);
    }
}
