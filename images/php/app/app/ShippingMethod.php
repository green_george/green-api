<?php

namespace App;

class ShippingMethod extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_shipping_methods';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @see https://goshippo.com/docs/reference#servicelevels
     */
    const SHIPPO_MAP = [
        // FedEx
        'fedex_ground' => 'FEDEX_GR',
        'fedex_2_day' => 'FEDEX_2D',
        'fedex_international_economy' => 'FEDEX_EC', // International Economy
        'fedex_express_saver' => 'FEDEX_ES', // Express Saver (3 Day)
        'fedex_first_overnight' => 'FEDEX_FO', // First Overnight
        'fedex_international_priority' => 'FEDEX_IP', // International Priority
        'fedex_priority_overnight' => 'FEDEX_PO', // Priority Overnight
        'fedex_standard_overnight' => 'FEDEX_SO', // Standard Overnight

        // UPS
        'ups_standard' => 'UPSST', // Standard
        'ups_next_day_air' => 'UPS_1DA', // Next Day Air
        'ups_next_day_air_early_am' => 'UPS_1DM', // Next Day Air Early
        'ups_next_day_air_saver' => 'UPS_1DP', // Next Day Air Saver
        'ups_second_day_air' => 'UPS_2DA', // 2nd Day Air
        'ups_second_day_air_am' => 'UPS_2DM', // 2nd Day Air Early AM
        'ups_3_day_select' => 'UPS_3DS', // 3 Day Select
        'ups_express_plus' => 'UPS_EP', // Worldwide Express Plus
        'ups_express' => 'UPS_ES', // Worldwide Express
        'ups_expedited' => 'UPS_EX', // Worldwide Expedited
        'ups_ground' => 'UPS_GND', // Ground
        'ups_saver' => 'UPS_SV', // Saver

        // USPS
        'usps_first' => 'USPS_FC', // First Class
        'usps_priority_mail_express_international' => 'USPS_PEI', // Priority Mail Express International
        'usps_priority_express' => 'USPS_PME', // Priority Mail Express
        'usps_parcel_select' => 'USPS_PP', // Parcel
        'usps_priority' => 'USPS_PRI', // Priority
    ];

    /**
     * Query our shipping methods using SHIPPO "service levels".
     */
    public function scopeMatchingShippoToken($query, $token)
    {
        $map = self::SHIPPO_MAP;
        $code = isset($map[$token]) ? $map[$token] : null;
        return $query->where('method_code', $code);
    }

    /**
     * Get the full name of this shipping method (includes the carrier).
     */
    public function scopeFindByFullName($query, $fullName)
    {
        return $query->whereRaw('CONCAT(method_carrier, " ", method_name) = ?', [ $fullName ])->firstOrFail();
    }

    /**
     * Get the full name of this shipping method (includes the carrier).
     */
    public function getFullNameAttribute()
    {
        return $this->method_carrier . ' ' . $this->method_name;
    }

    public function getShippoTokenAttribute()
    {
        $map = array_flip(self::SHIPPO_MAP);
        return $map[$this->method_code];
    }

    /**
     * Get the tracking url prefix.
     */
    public function getTrackingUrlPrefixAttribute()
    {
        $map = [
            'USPS' => 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=',
            'UPS'  => 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=',
            'FedEx' => 'http://www.fedex.com/Tracking?action=track&tracknumbers='
        ];
        return $map[$this->method_carrier];
    }
}
