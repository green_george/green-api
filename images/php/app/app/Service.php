<?php

namespace App;

class Service extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_services';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'service_price'  => 'float',
    ];

    /**
     * Get the Product models related to this Service.
     */
    public function products()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Product', 'wm_product_services')
        );
    }

    /**
     * Get the PricingTable models related to this Service.
     */
    public function pricingTables() {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\PricingTable', 'wm_service_pricing_tables')
        );
    }

    /**
     * Get the Discount models related to this Service.
     */
    public function discounts($customerId = -1)
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Discount', 'wm_service_discounts')->wherePivot('user_id', $customerId)
        );
    }

    /**
     * Get the prices for this Service.
     *
     * @param int $inCustomerId - Customer ID (used to get appropriate pricing tables)
     * @param int $quantity - (optional, defaults to one)
     * @param int $lookupQuantity - (optional, defaults to $quantity)
     * @return float - Unit price for this Service
     */
    public function getPrices($inCustomerId = -1, $quantity = 1, $lookupQuantity = null)
    {
        $lookupQuantity = $lookupQuantity ?: $quantity;

        // Get pricing table
        $pricingTable = $this->pricingTables->last();
        if ($pricingTable === null) abort(404, 'Missing pricing table for service');

        // Find the appropriate pricing tier for our quantity
        $tier = $pricingTable->findTier($lookupQuantity);
        if ($tier === null) abort(404, 'Missing a matching pricing table tier for service');
        $price = $tier->price * $quantity;

        // Apply discounts
        $discounts = $this->discounts($inCustomerId)->get();
        return (object) [
            'unit' => $tier->price,
            'total' => $discounts->applyTo($price, $price, false, $lookupQuantity),
        ];
    }
}
