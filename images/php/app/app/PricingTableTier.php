<?php

namespace App;

class PricingTableTier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_pricing_table_data';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'last_updated';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['store_id', 'pricing_table_id'];

    /**
     * Get the parent pricing table for the data.
     */
    public function pricingTable()
    {
        return $this->belongsTo('App\PricingTable');
    }

    /**
     * Get the tier price
     */
    public function getPriceAttribute()
    {
        return (float) $this->attributes['price'];
    }
}
