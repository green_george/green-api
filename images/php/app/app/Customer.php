<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_users';

    /**
     * The attributes that should be renamed.
     * (remove "user_" prefix)
     *
     * @var array
     */
    protected $mappedAttributes = [
        'user_availability' => 'availability',
        'user_email' => 'email',
        'user_name' => 'name',
        'user_pass' => 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'recovery_question',
        'recovery_answer',
    ];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'time_last';

    /**
     * The attributes that should be decrypted/encrypted.
     *
     * @var array
     */
    protected $encryptedAttributes = [
        'name',
        'email',
        'password',
        'recovery_question',
        'recovery_answer',
    ];

    /**
     * @return string[] UPS|FedEx|USPS
     */
    public function validProviders() {
        $userTerm = DB::table('wm_user_terms')
            ->where('store_id', 9)
            ->where('user_term_type', 2)
            ->where('user_id', $this->id)
            ->selectRaw('CASE WHEN user_term_desc LIKE "UPS%" THEN "UPS" WHEN user_term_desc LIKE "FedEx%" THEN "FedEx" ELSE NULL END AS `provider`')
            ->first();

        return ($userTerm && $userTerm->provider
            ? [ $userTerm->provider ]
            : [ 'USPS', 'UPS' ]
        );
    }

    /**
     * Get the Address models related to this Customer.
     */
    public function addresses()
    {
        return $this->hasMany('App\Address', 'user_id');
    }

    /**
     * Get the Address (shipping) models related to this Customer.
     */
    public function shippingAddresses()
    {
        return $this->addresses()->where('address_type', Address::SHIPPING_ADDRESS_TYPE);
    }

    /**
     * Get the Address (billing) models related to this Customer.
     */
    function billingAddresses()
    {
        return $this->addresses()->where('address_type', Address::BILLING_ADDRESS_TYPE);
    }

    /**
     * Get the Order models related to this Customer.
     */
    function orders()
    {
        return $this->hasMany('App\Order', 'user_id');
    }
}
