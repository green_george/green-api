<?php

namespace App;

class Discount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_discounts';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new DiscountCollection($models);
    }

    /**
     * Get the Service models related to this Discount.
     */
    public function services($customerId = -1)
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Service', 'wm_service_discounts')->wherePivot('user_id', $customerId)
        );
    }

    /**
     * Get the Product models related to this Discount.
     */
    /*
    // TODO
    public function services($customerId = -1)
    {
        // NOTE: This is a custom-built relation to also get Category-level discounts
        return new DiscountHasProducts($this, $customerId);
    }
    */
}
