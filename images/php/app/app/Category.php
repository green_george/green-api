<?php

namespace App;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_categories';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;

    public function products() {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Product', 'wm_category_products')
        );
    }

    public function discounts() {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Discount', 'wm_category_discounts')
        );
    }
}
