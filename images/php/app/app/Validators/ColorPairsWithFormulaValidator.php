<?php

namespace App\Validators;

use Illuminate\Support\Arr;

class ColorPairsWithFormulaValidator extends Validator
{

    /**
     * Validate an attribute.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Illuminate\Validation\Validator $validator
     * @return boolean
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        // Ex. $attribute === "products.0.numberOfColors"
        $formulaAttribute = $this->getPairedFormulaAttribute($attribute); // Ex. "products.0" + ".formula"
        $formulaValue = strtolower(
            Arr::get($validator->getData(), $formulaAttribute) // NOTE: Copied from Validator::getValue()
        );

        $isRestricted = in_array($formulaValue, [ 'foil', 'sublimate' ]);
        return ($isRestricted && $value === 0) || !$isRestricted;
    }

    /**
     * Replaces placeholder text (like ":attribute") in a failed validation message.
     *
     * @param string $message
     * @param string $attribute
     * @param string $rule
     * @param array $parameters
     * @return string
     */
    public function replaceInMessage($message, $attribute, $rule, $parameters)
    {
        $formulaAttribute = $this->getPairedFormulaAttribute($attribute);
        return str_replace(':formulaAttribute', $formulaAttribute, $message);
    }

    /**
     * Get "path.to.parent.formula" from "path.to.parent.someOtherAttribute"
     */
    protected function getPairedFormulaAttribute($attribute)
    {
        $arr = explode('.', $attribute); // break up the dot notation on the attribute into an array
        array_pop($arr); // remove last item (left with the path to the parent attribute, if any)
        array_push($arr, 'formula'); // then add "formula" as the last item (if no parent, formula is the only entry)
        return implode('.', $arr); // rebuild string as parent.formula, effectively getting
    }
}
