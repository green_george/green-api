<?php

namespace App\Validators;

abstract class Validator
{
    /**
     * Validate an attribute.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Illuminate\Validation\Validator $validator
     * @return boolean
     */
    abstract public function validate($attribute, $value, $parameters, $validator);

    /**
     * Replaces placeholder text (like ":attribute") in a failed validation message.
     *
     * @param string $message
     * @param string $attribute
     * @param string $rule
     * @param array $parameters
     * @return string
     */
    public function replaceInMessage($message, $attribute, $rule, $parameters) {
        // return str_replace(':attribute', $attribute, $message);
    }
}
