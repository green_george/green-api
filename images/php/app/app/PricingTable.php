<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;

class PricingTable extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_pricing_tables';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'last_updated';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['store_id', 'table_mode_id'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        $outerTable = (new self)->getTable();

        static::addGlobalScope('withModes', function (Builder $query) use ($outerTable) {
            $joinTable = 'wm_pricing_table_modes';
            $query
                ->join($joinTable, "{$joinTable}.id", '=', "{$outerTable}.table_mode_id")
                ->select("{$outerTable}.*", "{$joinTable}.mode_desc AS mode")
                ->latest('time_created')
                ->limit(1);
        });

        static::addGlobalScope('withTiers', function (Builder $query) {
            $query->with(['tiers']);
        });
    }

    /**
     * Get the PricingTableTier models related to this PricingTable.
     */
    public function tiers()
    {
        return $this->hasMany('App\PricingTableTier');
    }

    /**
     * Get the Product models related to this PricingTable.
     */
    public function products()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Product', 'wm_product_pricing_tables')
        );
    }

    /**
     * Get the Service models related to this PricingTable.
     */
    public function services()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Service', 'wm_service_pricing_tables')
        );
    }

    /**
     * Find the matching tier for a given quantity.
     */
    public function findTier($lookupQuantity = 1)
    {
        $lookupQuantity = (int) $lookupQuantity;
        return $this->tiers->first(function ($tier) use ($lookupQuantity) {
            return (
                $lookupQuantity >= $tier->first_quantity // start
                && ($lookupQuantity <= $tier->last_quantity || $tier->last_quantity === -1) // end
            );
        });
    }

}
