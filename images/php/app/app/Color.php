<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Color extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_colors';

    /**
     * Get the Product models related to this Color.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'wm_product_colors');
    }

    /**
     * Narrow query results down to colors that match the given printroId in wm_color_id_map.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $printroId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFromPrintroId(Builder $query, int $printroId)
    {
        // Query the wm_color_id_map table for the given printro_id value
        $rows = $this->getColorIdMapQuery()
            ->select('color_id')
            ->where('printro_id', $printroId)
            ->get();
        
        // Extract only the color_id's
        $colorIds = $rows->map(function($color) {
            return $color->color_id;
        });

        // Restrict the colors in this query to those color_id's found above
        return $query->whereIn('id', $colorIds);
    }

    /**
     * Get the printro_id value from the color id map.
     */
    public function getPrintroIdAttribute()
    {
        $row = $this->getColorIdMapQuery()->select('printro_id')->where('color_id', $this->id)->first();
        return $row->printro_id;
    }

    private function getColorIdMapQuery() {
        return DB::connection('wm_dashboard')->table('wm_color_id_map');
    }
}
