<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use App\Relations\ProductHasDiscounts;
use App\Traits\Meta;
use App\Service;

class Product extends Model
{
    use Meta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_products';

    /**
     * The attributes that should be renamed.
     * (remove "product_" prefix)
     *
     * @var array
     */
    protected $mappedAttributes = [
        'product_width' => 'width',
        'product_height' => 'height',
        'product_length' => 'length',
        'product_weight' => 'weight',
    ];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'last_updated';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_width'  => 'float',
        'product_height' => 'float',
        'product_length' => 'float',
        'product_weight' => 'float',
    ];

    /**
     * Create a new Eloquent Product model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        // Enable meta data to be searched and accessed
        $this->initMeta('wm_product_meta', null, [
            '_minimum_quantity' => 'integer',
        ]);

        parent::__construct($attributes);
    }

    /**
     * Get the mapped number_of_colors value from the meta.
     */
    public function getNumberOfColorsAttribute()
    {
        $this->loadMissing('meta');
        $isNumeric = preg_match('/^\d+/', $this->meta->{'@attribute:ink'}, $matches);
        return $isNumeric ? (integer) $matches[0] : 0;
    }

    /**
     * Get the unit, service and total prices for this Product.
     *
     * @param int $inCustomerId - Customer ID (used to get appropriate pricing tables)
     * @param int $quantity - (optional, defaults to one)
     * @param int $lookupQuantity - (optional, defaults to $quantity)
     * @param array $optionalServicesData - Optional services to apply to total price
     * @return object - w/ `unit` and `total` properties
     */
    public function getPrices($customerId = -1, $quantity = 1, $lookupQuantity = null, $optionalServicesData = [])
    {
        $lookupQuantity = $lookupQuantity ?: $quantity;

        // Get pricing table
        $pricingTable = $this->pricingTables->last();
        if ($pricingTable === null) abort(404, 'No pricing table found for product');

        // Find the appropriate pricing tier for our quantity
        $tier = $pricingTable->findTier($lookupQuantity);
        if ($tier === null) abort(404, 'No matching pricing tier found for product');

        // Calculate base prices
        $unitPrice = $tier->price;
        $subtotalPrice = $pricingTable->mode === 'PRECISE'
            ? $tier->price * $quantity
            : $tier->price; // NOTE: assuming 'ONETIME' here

        // Apply discounts
        $discounts = $this->discounts($customerId)->get();
        $discountedUnitPrice = $discounts->applyTo($unitPrice, $subtotalPrice, true, $quantity, $lookupQuantity);
        $discountedSubtotalPrice = $discounts->applyTo($subtotalPrice, $subtotalPrice, false, $quantity, $lookupQuantity);

        // Gather optional service models and their quantities
        $optionalServices = collect($optionalServicesData)->map(function($optionalServiceData) {
            $optionalServiceData = (object) $optionalServiceData;
            return [
                'service' => Service::find($optionalServiceData->id),
                'quantity' => $optionalServiceData->quantity,
            ];
        });

        // Gather required service models and their quantities (always 1)
        $requiredServices = $this->services($isRequired = true)->get()->map(function($service) {
            return [
                'service' => $service,
                'quantity' => 1,
            ];
        });

        // Calculate service price
        $servicePrice = $requiredServices->concat($optionalServices)
            ->sum(function ($serviceData) use ($subtotalPrice, $customerId, $lookupQuantity) {
                return $serviceData['service']->getPrices($customerId, $serviceData['quantity'], $lookupQuantity)->total;
            }, 0.00);

        // Return (possibly discounted) prices
        return (object) [
            'unit' => $discountedUnitPrice,
            'total' => $discountedSubtotalPrice + $servicePrice,
            'service' => $servicePrice,
        ];
    }

    /**
     * Get the OrderLineItem models related to this Product.
     */
    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct');
    }

    /**
     * Get the Category models related to this Product.
     */
    public function categories()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Category', 'wm_category_products')
        );
    }

    /**
     * Get the PricingTable models related to this Product.
     */
    public function pricingTables()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\PricingTable', 'wm_product_pricing_tables')
        );
    }

    /**
     * Get the Service models related to this Product.
     *
     * @param boolean $isRequired - optional
     */
    public function services($isRequired = null)
    {
        $query = $this->belongsToMany('App\Service', 'wm_product_services');
        if ($isRequired !== null) $query->wherePivot('is_required', (int) $isRequired);
        return static::storeIdOnPivot($query);
    }

    /**
     * Get the Discount models related to this Product.
     *
     * @param int $customerId
     */
    public function discounts(int $customerId = -1)
    {
        // NOTE: This is a custom-built relation to also get Category-level discounts
        return new ProductHasDiscounts($this, $customerId);
    }

    /**
     * Get the Color models related to this Product.
     */
    public function colors()
    {
        return $this->belongsToMany('App\Color', 'wm_product_colors');
    }

    /**
     * Narrow query results down to products that match the given values.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $values
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMatches(Builder $query, array $values)
    {
        // Match using formula meta
        if (isset($values['formula'])) {
            $this->scopeMetaExists($query, '@attribute:formula', $values['formula']);
        }

        // Match using numberOfColors meta
        if (isset($values['numberOfColors'])) {
            $numOfColors = (integer) $values['numberOfColors'];

            $queryValues = $numOfColors === 0
                ? [ 'foil', 'sublimate', 'freedom', 'PR' ] // Non-numeric
                : [ "{$numOfColors}color", "{$numOfColors}C" ]; // 1 color, 2 color, etc.

            $this->scopeMetaExists($query, '@attribute:ink', $queryValues);
        }

        // Determine the width and height that best fits the provided design
        if (isset($values['width']) && isset($values['height'])) {
            $width = (float) $values['width'];
            $height = (float) $values['height'];
            $query
                ->whereRaw('LEAST(product_width, product_height) >= ?', [ min($width, $height) ])
                ->whereRaw('GREATEST(product_width, product_height) >= ?', [ max($width, $height) ]);
        }

        return $query;
    }

    /**
     * Narrow query results down to a single product with the smallest dimensions.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \App\Product
     */
    public function scopeFindSmallestOrFail(Builder $query)
    {
        $query->orderByRaw('LEAST(product_width, product_height), GREATEST(product_width, product_height)');
        return $query->firstOrFail();
    }

    /**
     * Narrow query results down to a single product with the lowest price.
     *
     * NOTE: DO NOT remove typehinting on $quantity, this prevents SQL injection attacks.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $quantity
     * @return \App\Product
     */
    public function scopeFindLowestPriceOrFail(Builder $query, int $quantity)
    {
        // NOTE: This was built assuming we are using this with scopeMatches...
        // Take original query, join it with the pricing table tiers to get prices for the given quantity
        // Then, order by lowest price
        // Finally, get the first (thus, lowest priced) product's id
        $pk = 'wm_products.id';
        $id = $query // Starting with any previously-added query restrictions
            ->selectRaw(
                <<<HEREDOC
                $pk AS id,
                ((
                    /* (Price at the given quantity...) */
                    SELECT ptd.price * $quantity
                    FROM
                        wm_product_pricing_tables ppt
                        INNER JOIN wm_pricing_table_data ptd ON ptd.pricing_table_id = ppt.pricing_table_id
                    WHERE
                        ppt.product_id = $pk
                        AND $quantity >= ptd.first_quantity
                        AND (ptd.last_quantity = -1 OR $quantity <= ptd.last_quantity)
                    /* (...using the most recently created pricing table) */
                    ORDER BY ppt.id DESC
                    LIMIT 1
                /* ... + Price of required services for that product */
                ) + COALESCE((
                    SELECT SUM(ptd.price)
                    FROM
                        wm_product_services ps
                        INNER JOIN wm_service_pricing_tables spt ON spt.service_id = ps.service_id
                        INNER JOIN wm_pricing_table_data ptd ON ptd.pricing_table_id = spt.pricing_table_id
                    WHERE
                        ps.product_id = $pk
                        AND $quantity >= ptd.first_quantity
                        AND (ptd.last_quantity = -1 OR $quantity <= ptd.last_quantity)
                ), 0)) AS `total`
                HEREDOC
            )
            ->havingRaw('total IS NOT NULL')
            ->orderBy('total', 'ASC')
            ->firstOrFail()
            ->id;

        // Get the product
        return self::findOrFail($id);
    }
}
