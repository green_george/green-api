<?php

namespace App;

class OrderProduct extends Model
{
    const PRODUCTION_TYPE_MAP = [
        ''              => -1,
        'heat_transfer' => 2,
        'digital'       => 3,
        'outsourced'    => 5,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_order_products';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     * @see https://laravel.com/docs/5.7/eloquent#mass-assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'unit_tax'  => 'float',
        'unit_price'  => 'float',
        'total_price'  => 'float',
    ];

    /**
     * Default values for attributes
     * @var  array an array with attribute as key and default as value
     */
    protected $attributes = [
        'store_id' => 9,
        'production_type' => -1,
        'production_status' => -1,
        'unit_tax' => 0.0,
    ];

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // First, calculate and save unit price and total price
        if (!isset($this->attributes['unit_price']) && !isset($this->attributes['total_price'])) {
            $prices = $this->getPrices($this->order->user_id);
            $this->unit_price = $prices->unit;
            $this->total_price = $prices->total;
        }

        // Next, figure out the production_type
        /*
        // NOTE: For now, this will be handled externally
        $this->production_type = self::PRODUCTION_TYPE_MAP[
            $this->product_data->attribute['production'] ?? ''
        ];
        */

        // Then, save like normal
        return parent::save($options);
    }

    /**
     * Get the product designs as an unserialized array.
     *
     * @param  string  $value
     * @return Collection
     */
    public function getProductDesignsAttribute($value)
    {
        $unserializedValue = $value !== null ? unserialize($value) : [];
        return collect($unserializedValue)->map(function($design) {
            /*
                Example $design:
                --------
                Array (
                    "id" => "493780",
                    "height" => 3.5,
                    "width" => 11.5,
                    "gutter" => "",
                    "file_id" => "4461312",
                    "design_size_id" => 1292714,
                    "@color:c1" => "16922",
                    "rotation" => "[{\"pass\":\"1\",\"ink_color_id\":\"16922\",\"mesh\":\"110\",\"finishing\":\"Powder 6P82\"}]",
                    "print_ready_file" => 4461369,
                    "print_ready_file_name" => "720997-1.pdf",
                    "hot_folder" => "GIT-TD-1C-12",
                    "pace_job" => "S674586",
                    "pace_job_part" => "01",
                    "pace_run" => "R184948"
                )
            */

            // Enforce schema by setting defaults
            $updatedDesign = green_array_unflatten($design, [
                'printro_id' => null,
                'placement'  => 'center',
                'color' => [],
                'field' => [],
                'data' => [],
            ]);

            // Fix types
            $updatedDesign['id'] = (float) $updatedDesign['id'];
            $updatedDesign['rotation'] = isset($updatedDesign['rotation']) ? json_decode($updatedDesign['rotation']) : null;
            if (isset($updatedService['height'])) $updatedService['height'] = (float) $updatedDesign['unit_price'];
            if (isset($updatedService['width'])) $updatedService['width'] = (float) $updatedDesign['total_price'];
            $updatedDesign['color'] = (object) $updatedDesign['color'];
            $updatedDesign['field'] = (object) $updatedDesign['field'];
            $updatedDesign['data'] = (object) $updatedDesign['data'];

            // Return the design array
            return (object) $updatedDesign;
        });
    }

    /**
     * Set the product designs as a serialized array.
     *
     * @param  array|object  $designs
     */
    public function setProductDesignsAttribute($designs)
    {
        $this->attributes['product_designs'] = serialize(collect($designs)->map(function($design) {
            return green_array_flatten($design);
        })->all());
    }

    /**
     * Get the unserialized product services.
     *
     * @param  string  $value
     * @return object
     */
    public function getProductServicesAttribute($value)
    {
        $unserializedValue = $value !== null ? unserialize($value) : [];
        return collect($unserializedValue)->map(function($service) {
            /*
                Example $service:
                --------
                Array (
                    "id" => 6,
                    "quantity" => 1,
                    "total" => "25.00", // redundant
                    "price" => "25.00", // redundant
                    "unit_price" => "25.00",
                    "total_price" => "25.00"
                )
            */

            // Enforce schema by setting defaults
            $updatedService = green_array_unflatten($service, [
                'unit_price' => 0,
                'total_price' => 0,
                'quantity' => 0,
            ]);

            // Remove redundant data
            unset($updatedService['total']);
            unset($updatedService['price']);

            // Enforce/Fix types
            $updatedService['unit_price'] = (float) $updatedService['unit_price'];
            $updatedService['total_price'] = (float) $updatedService['total_price'];
            $updatedService['quantity'] = (int) $updatedService['quantity'];

            // Return as an object
            return (object) $updatedService;
        });
    }

    /**
     * Set the product services as a serialized array.
     *
     * @param  array|object  $designs
     */
    public function setProductServicesAttribute($services)
    {
        $this->attributes['product_services'] = serialize(collect($services)->map(function($service) {
            return green_array_flatten($service);
        })->all());
    }

    /**
     * Get the unserialized product data.
     *
     * @param  string  $value
     * @return object
     */
    public function getProductDataAttribute($value)
    {
        // Unserialize
        $productData = $value !== null ? unserialize($value) : [];

        // Expand/unflatten "@parentKey:" keys
        return (object) green_array_unflatten($productData);
    }

    /**
     * Set the product data as a serialized array.
     *
     * @param  array|object  $designs
     */
    public function setProductDataAttribute($data)
    {
        $this->attributes['product_data'] = serialize(green_array_flatten($data));
    }

    /**
     * Get the unit and total price for this Product.
     *
     * @return object - w/ `unit` and `total` properties
     */
    public function getPrices($user_id, $includeServicePrice = false)
    {
        if (!$this->product_quantity) throw new \Exception('Cannot get prices - OrderProduct has no set quantity.');

        // If we don't need the service price, we don't need to do any calculations...just spit out the prices as-is
        if (!$includeServicePrice && isset($this->attributes['unit_price']) && isset($this->attributes['total_price'])) {
            return (object) [
                'unit' => $this->unit_price,
                'total' => $this->total_price,
            ];
        }

        return $this->product->getPrices($user_id, $this->product_quantity, null, $this->product_services->all());
    }

    /**
     * Get the Order models related to this OrderLineItem.
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Get the Product models related to this OrderLineItem.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
