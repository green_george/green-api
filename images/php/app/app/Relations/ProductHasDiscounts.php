<?php
namespace App\Relations;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Discount;

/*
@see https://medium.com/@codebyjeff/custom-pivot-table-models-or-choosing-the-right-technique-in-laravel-fe435ce4e27e
NOTE: May be better to use a Pivot class?
In Discount.php: $this->belongsToMany('App\Product')->using('App\ProductDiscountPivot')
In Product.php: $this->belongsToMany('App\Discount')->using('App\ProductDiscountPivot')
*/
class ProductHasDiscounts extends Relation
{
    protected $categoryDiscountForeignKey = 'wm_category_products.product_id';
    protected $productDiscountForeignKey = 'wm_product_discounts.product_id';
    protected $foreignKey = 'product_id';
    protected $localKey = 'id';

    protected $categoryDiscountQuery; // To be unioned with productDiscountQuery to form $this->query
    protected $productDiscountQuery; // To be unioned with categoryDiscountQuery to form $this->query

    /**
     * Get the key value of the parent's local key.
     *
     * @return mixed
     */
    public function getParentKey()
    {
        return $this->parent->getAttribute($this->localKey);
    }

    public function finalizeQuery() {
        $subquery = $this->productDiscountQuery->union($this->categoryDiscountQuery);
        $alias = 's';

        $this->query->joinSub($subquery, $alias, function($join) use ($alias) {
            $join->on("$alias.discount_id", '=', 'wm_discounts.id');
        });
    }

    /**
     * Create a new "has discounts" relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param string $customerId
     * @return void
     */
    public function __construct(Model $parent, $customerId)
    {
        $this->categoryDiscountQuery = DB::table('wm_category_discounts')
            ->join('wm_category_products', 'wm_category_products.category_id', '=', 'wm_category_discounts.category_id')
            ->where('wm_category_discounts.user_id', $customerId)
            ->select('wm_category_discounts.discount_id', "{$this->categoryDiscountForeignKey} AS {$this->foreignKey}");

        $this->productDiscountQuery = DB::table('wm_product_discounts')
            ->where('wm_product_discounts.user_id', $customerId)
            ->select('wm_product_discounts.discount_id', "{$this->productDiscountForeignKey} AS {$this->foreignKey}");

        parent::__construct(Discount::query(), $parent);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        // For a single parent
        if (static::$constraints) {
            $parentKey = $this->getParentKey();
            $this->categoryDiscountQuery->where($this->categoryDiscountForeignKey, $parentKey);
            $this->productDiscountQuery->where($this->productDiscountForeignKey, $parentKey);
            $this->finalizeQuery();
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        // For multiple parents
        $this->categoryDiscountQuery->whereIn($this->categoryDiscountForeignKey, $this->getKeys($models, $this->localKey));
        $this->productDiscountQuery->whereIn($this->productDiscountForeignKey, $this->getKeys($models, $this->localKey));
        $this->finalizeQuery();
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $this->related->newCollection());
        }
        return $models;
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array   $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        $dictionary = $results->mapToDictionary(function ($result) {
            $result->makeHidden('product_id');
            return [$result->{$this->foreignKey} => $result];
        })->all();

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        foreach ($models as $model) {
            if (isset($dictionary[$key = $model->getAttribute($this->localKey)])) {
                $model->setRelation(
                    $relation, $this->related->newCollection($dictionary[$key])
                );
            }
        }
        return $models;
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get($columns = ['*'])
    {
        // First we'll add the proper select columns onto the query so it is run with
        // the proper columns. Then, we will get the results and hydrate out pivot
        // models with the result of those columns as a separate model relation.
        $columns = $this->query->getQuery()->columns ? [] : $columns;
        if ($columns == ['*']) {
            $columns = [$this->related->getTable().'.*'];
        }
        if (!in_array($this->foreignKey, $columns))
            array_push($columns, $this->foreignKey);
        $builder = $this->query->applyScopes();
        $models = $builder->addSelect($columns)->getModels();

        // If we actually found models we will also eager load any relationships that
        // have been specified as needing to be eager loaded. This will solve the
        // n + 1 query problem for the developer and also increase performance.
        if (count($models) > 0) {
            $models = $builder->eagerLoadRelations($models);
        }

        return $this->related->newCollection($models);
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return $this->query->get();
    }

}
