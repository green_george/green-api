<?php
namespace App\Relations;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class HasMeta extends Relation
{
    protected $metaClass = '\App\Meta';
    protected $table;
    protected $foreignKey;
    protected $localKey;
    protected $casts = [];

    /**
     * Get the key value of the parent's local key.
     *
     * @return mixed
     */
    public function getParentKey()
    {
        return $this->parent->getAttribute($this->localKey);
    }

    /**
     * Create a new "has meta" relationship instance.
     *
     * @param string $table
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param string $foreignKey
     * @param string $localKey
     * @param array $casts - Passed on to Meta model
     * @return void
     */
    public function __construct(string $table, Model $parent, $foreignKey = null, $localKey = 'id', $casts = [])
    {
        $this->table = $table;
        $this->foreignKey = $foreignKey ?: snake_case(class_basename($parent)) . '_id';
        $this->localKey = $localKey;
        $this->query = DB::table($table)->select($this->foreignKey, 'meta_key', 'meta_value');
        $this->parent = $parent;
        $this->casts = $casts;
        $this->addConstraints();
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        // For a single parent
        if (static::$constraints) {
            $this->query->where($this->foreignKey,  $this->getParentKey());
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        // For multiple parents
        $this->query->whereIn($this->foreignKey, $this->getKeys($models, $this->localKey));
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array   $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation,
                (new $this->metaClass())->setTable($this->table)->setForeignKey($this->foreignKey, $model->id)
            );
        }
        return $models;
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array   $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        $dictionary = $results->mapWithKeys(function ($result) {
            $fk = $result[$this->foreignKey];
            return [ $fk => $result ];
        });

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        foreach ($models as $model) {
            if (isset($dictionary[$key = $model->getAttribute($this->localKey)])) {
                $related = $model->getRelation($relation);
                $model->setRelation($relation, $this->initMetaInstance($related, $model)->forceFill($dictionary[$key] ?? []));
            }
        }
        return $models;
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get($columns = ['*'])
    {
        return new \Illuminate\Database\Eloquent\Collection($this->getDictionary($columns)->values());
    }

    private function getDictionary($columns = ['*'])
    {
        return $this->query->get($columns)->mapToDictionary(function ($result) {
            $fk = $result->{$this->foreignKey};
            return [ $fk => $result ];
        })->map(function ($metaPairs, $key) {
            $metaArray = array_map(function ($metaPair) {
                return (array) $metaPair;
            }, $metaPairs);

            $metaData = array_combine(
                array_column($metaArray, 'meta_key'),
                array_column($metaArray, 'meta_value')
            );

            return [ $this->foreignKey => $key ] + $metaData;
        });
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        $metaData = $this->getDictionary()->first();
        $metaModel = $this->initMetaInstance()->forceFill($metaData);
        return $metaModel;
    }

    /**
     * Attach a model instance to the parent model.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Model|false
     */
    public function save(Model $model)
    {
        $model = $this->initMetaInstance($model);
        return $model->save() ? $model : false;
    }

    /**
     * Make a new related instance.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function newRelatedInstance()
    {
        return $this->initMetaInstance();
    }

    protected function initMetaInstance($meta = null, $parent = null)
    {
        $meta = $meta === null ? new $this->metaClass() : $meta;
        $parent = $parent === null ? $this->parent : $parent;
        return $meta
            ->setCasts($this->casts)
            ->setTable($this->table)
            ->setForeignKey($this->foreignKey, $parent->{$this->localKey});
    }
}
