<?php

namespace App;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use Shippo_Shipment;
use GuzzleHttp\Client as GuzzleClient;
use App\ShippingMethod;
use App\Address;
use App\Other\StatusTree;
use App\Traits\Meta;

class Order extends Model
{
    use Meta;

    const BOX_TYPES = [
        'standard' => [
            'length' => 12,
            'width' => 12,
            'height' => 12,
            'weight' => .6,
            'maxSheets' => 1000,
        ],
        'larger'   => [
            'length' => 12,
            'width' => 12,
            'height' => 12,
            'weight' => 1,
            'maxSheets' => 500,
        ],
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_orders';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'time_updated';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'time_donotshipbefore' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'ship_price'  => 'float',
    ];

    /**
     * Default values for attributes
     * @var  array an array with attribute as key and default as value
     */
    protected $attributes = [
        'user_order_number' => '-99',
        'comments' => '',
        'store_id' => 9,
        'type' => 1,
        'status' => -1,
        'integration_order_id' => '',
        'time_inhandsby' => -1,
        'time_cancelby' => -1,
        'time_donotshipbefore' => -1,
        'origin' => 'Printro API',
    ];

    /**
     * The attributes that should be renamed.
     * (remove "order_" prefix)
     *
     * @var array
     */
    protected $mappedAttributes = [
        'order_origin' => 'origin',
        'order_type' => 'type',
        'order_status' => 'status',
    ];

    /**
     * The attributes that aren't mass assignable.
     * @see https://laravel.com/docs/5.7/eloquent#mass-assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        // Enable meta data to be searched and accessed
        $this->initMeta('wm_order_meta', null, [
            '_estimated_arrival_date' => 'date:U',
        ]);

        // In development, we don't have any order progression, so let's default to a "PREFLIGHT IN PROGRESS" status
        $inDev = config('constants.IN_DEVELOPMENT');
        if ($inDev) $this->attributes['status'] = 9;

        parent::__construct($attributes);
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Determine if this is the first time saving this order
        $firstTimeSave = !isset($this->id);

        // Update the ship_price attribute if not set
        if (!array_key_exists('ship_price', $this->attributes))
            $this->ship_price = $this->getShippingRate()->price;

        // Then, save like normal (return false if failed)
        $success = parent::save($options);
        if (!$success) return $success;

        // If this is the first time saving...
        if ($firstTimeSave) {
            // Preemptively replicate addresses, type = 90 to make sure we have a never-changing address
            /*
            $newAddress = $this->shippingAddress->replicate();
            $newAddress->address_type = 90;
            $newAddress->save();
            */
        }

        // Replace temporary value
        /*
        if ($this->user_order_number === '-99') {
            $this->user_order_number = (string) $this->id;
            $success = parent::save($options);
        }
        */
        return $success;
    }

    /**
     * Saves this Order, then saves the provided OrderProduct models.
     * (Sets the OrderProduct models on the Order first, so save() can properly calculate shipping)
     *
     * @param \Illuminate\Support\Collection $orderProducts
     * @return bool - Whether or not the saves were successful.
     */
    public function saveWithOrderProducts(Collection $orderProducts)
    {
        // Assign OrderProducts to the new order
        $this->setRelation('orderProducts', $orderProducts);

        // Save the Order model
        $success = $this->save();
        if (!$success) return $success;

        // Link OrderProducts to this Order on save
        $this->orderProducts()->saveMany($orderProducts);

        // Call url to finish the order creation logic in the existing system
        // TODO: Move whatever logic is run in this cleanup script into this API
        if (config('constants.IN_PRODUCTION')) {
            (new GuzzleClient())->get("http://dashboard.greendistro.com/dev-test/api/orderCleanup.php?orderId={$this->id}&verify=brydon");
        }
        return $success;
    }

    /**
     * Narrow query results down to orders that match the given status values.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $groupName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatusGroup($query, $groupName)
    {
        $isOpenOrder = in_array($groupName, [ StatusTree::SHIPPED, StatusTree::OPEN ]);

        // Get the statuses using the StatusTree (Ex. Closed -> FULFILLED)
        $statusTree = new StatusTree();
        $statuses = $statusTree->getStatusesForGroup($groupName);

        // Removes status 3 (so it can be used in faked SHIPPED status below)
        // TODO: Remove faked SHIPPED status
        $pos = array_search(3, $statuses);
        $includeStatus3 = $pos !== false;
        if ($includeStatus3) unset($statuses[$pos]);

        // Limit query to the provided statuses
        $query = $query->whereIn('order_status', $statuses);

        // TODO: Remove faked SHIPPED status
        if ($includeStatus3) {
            return $query->orWhere(function($query) use ($isOpenOrder) {
                $query->where('order_status', 3);
                $op = $isOpenOrder ? '>' : '<=';
                $query->whereRaw("time_donotshipbefore $op UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 day))");
            });
        }

        return $query;
    }

    /**
     * Get the status name from STATUS_MAP based on the status code.
     */
    public function getStatusNameAttribute()
    {
        return StatusTree::STATUS_MAP[$this->status] ?? $this->status;
    }

    /**
     * Get the first-level status group name.
     */
    public function getStatusGroup1Attribute()
    {
        $shippedDate = (int) $this->time_donotshipbefore->format('U');
        if ($shippedDate < date('U', strtotime('-7 days')) && $this->status === 3)
            return StatusTree::FULFILLED;

        return (new StatusTree())->getGroupForStatus($this->status, 1);
    }

    /**
     * Get the second-level status group name.
     */
    public function getStatusGroup2Attribute()
    {
        $shippedDate = (int) $this->time_donotshipbefore->format('U');
        if ($shippedDate < date('U', strtotime('-7 days')) && $this->status === 3)
            return StatusTree::CLOSED;

        return (new StatusTree())->getGroupForStatus($this->status, 2);
    }

    /**
     * Get the packages list.
     */
    public function getPackagesAttribute()
    {
        $shipDatePlus7 = $this->time_donotshipbefore->addDays(7);

        return $this->shipments->flatMap(function ($shipment) use ($shipDatePlus7) {
            return $shipment->packages->map(function($package) use ($shipDatePlus7) {
                $addedData = $this->status_group2 === StatusTree::OPEN
                    ? [ 'estimatedDeliveryDate' => $this->meta->_estimated_arrival_date ?? $shipDatePlus7 ]
                    : [ 'deliveryDate' => $shipDatePlus7 ];

                return [
                    'shippingMethod' => isset($package->shippingMethod) ? $package->shippingMethod->full_name : '',
                    'trackingNumber' => $package->package_tracking,
                    'trackingUrl' => $package->tracking_url,
                ] + $addedData;
            });
        });
    }

    /**
     * Get the shipping price.
     */
    public function getShipPriceAttribute($value)
    {
        return $value ?? $this->getShippingRate()->price;
    }

    /**
     * Gets the prices for this order.
     */
    public function getPrices()
    {
        if(count($this->orderProducts) === 0) $this->load('orderProducts');

        $prices = $this->orderProducts->reduce(function($runningTotals, $orderProduct) {
            $tax = ($orderProduct->unit_tax * $orderProduct->product_quantity);
            $prices = $orderProduct->getPrices($this->user_id);
            return [
                'subtotal' => $runningTotals['subtotal'] + $prices->total,
                'tax' => $runningTotals['tax'] + $tax,
                'total' => $runningTotals['total'] + $prices->total + $tax,
            ];
        }, [
            'subtotal' => 0.00,
            'tax' => 0.00,
            'total' => 0.00,
        ]);
        $shippingPrice = (float) $this->ship_price;
        $prices['total'] += $shippingPrice;
        $prices['total'] = round($prices['total'], 2);
        return $prices + [ 'ship_price' => $shippingPrice ];
    }

    /**
     * Calculate and return all possible shipping method rates for the shippingAddress + orderProducts combination.
     *
     * @return \Illuminate\Support\Collection - Collections of Shippo_Object's (the rate object).
     */
    public function getShippingRates()
    {
        /** @see https://app.goshippo.com/settings/api */
        $address = $this->shippingAddress;
        $shipment = Shippo_Shipment::create([
            'async' => false,
            'address_from' => Address::getShippingDepartmentAddress()->toShippoFormatArray(),
            'address_to' => $address->toShippoFormatArray(),
            'parcels' => $this->getShippoParcels(),
        ]);

        // String[] where each value is: UPS|FedEx|USPS
        $validProviders = $this->customer->validProviders();

        // Rates are stored in the `rates` array inside the shipment object
        return collect($shipment['rates'])
            ->filter(function($rate) use ($validProviders) {
                return in_array($rate->provider, $validProviders);
            })
            ->map(function ($rate) {
            $estimatedDeliveryDate = Carbon::now()
                ->addDays($rate->estimated_days)
                ->format(config('constants.PRINTRO_DATE_FORMAT'));

            $shippingMethod = ShippingMethod::matchingShippoToken($rate->servicelevel->token)->first();

            return (object) [
                'method' => $shippingMethod ? $shippingMethod->fullName : null,
                'price' => (float) $rate->amount,
                'estimated_delivery' => $estimatedDeliveryDate,
            ];
        })->filter(function ($rate) {
            return $rate->method !== null;
        });
    }

    /**
     * Determines box dimensions and weight needed to fit orderProducts and builds an array of parcels.
     *
     * @return array
     */
    public function getShippoParcels()
    {
        $packageDetails = $this->getPackageDetails();

    	// Build an array of our evenly weighted boxes
    	return array_fill(0, $packageDetails['quantity'], array_only($packageDetails, [
            'length',
            'width',
            'height',
            'weight',
        ]) + [
            'distance_unit' => 'in',
            'mass_unit'=> 'lb',
        ]);
    }

    /**
     * Determines box dimensions, weight and quantity needed to fit orderProducts.
     *
     * @return array
     */
    public function getPackageDetails()
    {
        // Loop through orderProducts to get the appropriate boxType, and to sum up the total weight and quantities
        [ $boxTypeKey, $totalWeight, $totalQuantity ] = $this->orderProducts->reduce(function ($orderDetails, $orderProduct) {
            $product = $orderProduct->product;
            $quantity = $orderProduct->product_quantity;
            return [
                /* $boxTypeKey    */ ($product->width * $product->height) > 225 ? 'larger' : $orderDetails[0],
                /* $totalWeight   */ $orderDetails[1] + ($product->weight * $quantity),
                /* $totalQuantity */ $orderDetails[2] + $quantity,
            ];
        }, [ 'standard', 0, 0 ]); // $boxTypeKey, $totalWeight, $totalQuantity default/initial values
        $box = static::BOX_TYPES[$boxTypeKey];
        $maxWeight = $box['weight'] + 66; // Smallest max weight, which happens to be USPS's international max weight: http://pe.usps.com/text/imm/immpg.htm

    	// Then, divvy the pieces into separate boxes ("packing them")
    	$totalBoxes = ceil($totalQuantity / $box['maxSheets']); // we'll need this many boxes
    	$weightPerBox = $totalWeight / $totalBoxes; // packing each box evenly, each box will weigh this much

        // If our weight per box is larger than the max weight, we need to split off that extra weight into more boxes
    	if ($weightPerBox > $maxWeight) {
    		$leftoverWeight = ($weightPerBox - $maxWeight) * $totalBoxes; // we have this much leftover that is too heavy
    		$extraBoxes = ceil($leftoverWeight / $maxWeight); // we'll need this many extra boxes to accommodate the extra weight
    		$totalBoxes += $extraBoxes; // our new number of boxes
    		$weightPerBox = $totalWeight / $totalBoxes; // our new weight per box
    	}

        // Return box dimensions and quantity
        return [
            'length' => $box['length'],
            'width' => $box['width'],
            'height' => $box['height'],
            'weight' => $weightPerBox + $box['weight'],
            'quantity' => $totalBoxes,
        ];
    }

    /**
     * Calculate and return shipping rate for the shippingMethod + shippingAddress + orderProducts combination.
     *
     * @param string|null $shippingMethod - Defaults to $this->ship_method when null.
     * @return Shippo_Object - The rate object from Shippo.
     */
    public function getShippingRate($shippingMethod = null)
    {
        $inShippingMethod = $shippingMethod ?? $this->ship_method;
        $shippingMethodModel = \App\ShippingMethod::where('method_code', $inShippingMethod)->firstOrFail();
        $fullName = $shippingMethodModel->full_name;
        $matchingRate = $this->getShippingRates()->first(function ($rate) use ($fullName) {
            return $rate->method === $fullName;
        });
        if (!$matchingRate) abort(404, 'Shipping Method not found');
        return $matchingRate;
    }

    /**
     * Get the Customer model related to this Order.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'user_id');
    }

    /**
     * Get the OrderLineItem models related to this Order.
     */
    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct')->where('product_quantity', '>', 0);
    }

    /**
     * Get the shipping Address model related to this Order.
     */
    public function shippingAddress()
    {
        return $this->belongsTo('App\Address', 'ship_to_id');
    }

    /**
     * Get the billing Address model related to this Order.
     */
    public function billingAddress()
    {
        return $this->belongsTo('App\Address', 'bill_to_id');
    }

    /**
     * Get the Shipment models related to this Order.
     */
    public function shipments()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Shipment', 'wm_order_shipments')
        );
    }
}
