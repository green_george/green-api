<?php

namespace App\Http\Controllers\Printro;

use App\Http\Controllers\Controller;
use App\Http\Requests\Printro\TransferPricesCalculationRequest;
use App\Http\Resources\Printro\TransferPricesResource;

class ProductController extends Controller
{
    /**
     * Display address list for provided user.
     *
     * @param  \App\Http\Requests\Printro\TransferPricesCalculationRequest  $request
     * @return \App\Http\Resources\Printro\TransferPricesResource
     */
    public function calculateTransferPrices(TransferPricesCalculationRequest $request)
    {
        // Re-map the services keys, changing "serviceId" to "id"
        $services = collect($request->input('services'))->map(function($serviceData) {
            return [
                'id' => $serviceData['serviceId'],
                'quantity' => $serviceData['quantity'],
            ];
        })->all();

        // Get the product that matched in the TransferPricesCalculationRequest validation steps
        $product = $request->matchingProduct;

        // Calculate unit, service and total prices for the matched product
        $prices = $product->getPrices(
            $request->input('userId'),
            $request->input('quantity'),
            null,
            $services
        );

        // Return a response with the prices and matched product info
        return new TransferPricesResource((object) [

            // Prices
            'unit_price' => $prices->unit,
            'service_price' => $prices->service,
            'total_price' => $prices->total,

            // Product
            'product_id' => $product->id,
            'product_name' => $product->product_name,
        ]);
    }
}
