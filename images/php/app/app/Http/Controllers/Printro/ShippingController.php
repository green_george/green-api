<?php

namespace App\Http\Controllers\Printro;

use App\Http\Controllers\Controller;
use App\Http\Requests\Printro\TransferShippingMethodListReadRequest;
use App\Http\Resources\Printro\ShippingMethodRateCollection;
use App\Order;
use App\OrderProduct;
use App\Product;

class ShippingController extends Controller
{
    /**
     * Calculate and return shipping method rates for customer + transfers.
     *
     * @param  \App\Http\Requests\Printro\TransferShippingMethodListReadRequest  $request
     * @return \App\Http\Resources\Printro\ShippingMethodRateCollection
     */
    public function calculateShippingMethodRates(TransferShippingMethodListReadRequest $request)
    {
        // Create an Order model
        $order = new Order([
            'user_id' => $request->input('userId'),
            'ship_to_id' => $request->input('addressId'),
        ]);

        // Create an OrderProduct model for each item in "products"
        $orderProducts = collect($request->input('products'))->map(function($productData) {
            $orderProduct = new OrderProduct([ 'product_quantity' => $productData['quantity'] ]);
            $orderProduct->setRelation('product', Product::matches($productData)->findLowestPriceOrFail($productData['quantity']));
            return $orderProduct;
        });

        // Attach the OrderProduct models to the Order model
        $order->setRelation('orderProducts', $orderProducts);

        // Use the Order model logic to calculate shipping rates
        return new ShippingMethodRateCollection($order->getShippingRates());
    }
}
