<?php

namespace App\Http\Controllers\Printro;

use App\Http\Controllers\Controller;
use App\Http\Requests\Printro\UserOrderListReadRequest;
use App\Http\Requests\Printro\OrderReadRequest;
use App\Http\Requests\Printro\TransferOrderCreateRequest;
use App\Http\Requests\Printro\TransferOrderPricesCalculationRequest;
use App\Http\Resources\Printro\OrderCollection;
use App\Http\Resources\Printro\OrderResource;
use App\Http\Resources\Printro\OrderIdentifierResource;
use App\Http\Resources\Printro\TransferOrderPricesResource;
use App\Http\Resources\Printro\OrderSummaryCollection;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\Color;
use App\Address;
use App\Service;
use App\ShippingMethod;
use App\Design;
use DB;
use GuzzleHttp\Client as GuzzleClient;
use App\Other\StatusTree;

class OrderController extends Controller
{
    /**
     * Calculate and show prices for an order.
     *
     * @param  \App\Http\Requests\Printro\TransferOrderCreateRequest  $request
     * @return \App\Http\Resources\Printro\OrderIdentifierResource
     */
    public function calculatePrices(TransferOrderPricesCalculationRequest $request)
    {
        // Create an Order model
        $order = new Order([
            'user_id'     => $request->input('userId'),
            'ship_to_id'  => $request->input('shippingAddressId'),
            'ship_method' => ShippingMethod::findByFullName($request->input('shippingMethod'))->method_code,
        ]);

        // Create an OrderProduct model for each item in "orderProducts"
        $orderProducts = collect($request->matchingProducts)->map(function($productData) {
            $services = collect($productData['services'])->map(function($serviceData) {
                return [
                    'id' => $serviceData['serviceId'],
                    'quantity' => $serviceData['quantity'],
                ];
            })->all();

            $orderProduct = new OrderProduct([
                'product_quantity' => $productData['quantity'],
                'product_services' => $services,
            ]);

            $orderProduct->setRelation('product', $productData['product']);

            return $orderProduct;
        });

        // Attach the OrderProduct models to the Order model
        $order->setRelation('orderProducts', $orderProducts);

        return new TransferOrderPricesResource((object) $order->getPrices());
    }

    /**
     * Store order for provided user.
     *
     * @param  \App\Http\Requests\Printro\TransferOrderCreateRequest  $request
     * @return \App\Http\Resources\Printro\OrderIdentifierResource
     */
    public function store(TransferOrderCreateRequest $request)
    {
        $customerId = $request->input('userId');

        // Get array of submitted orderProduct designs
        $orderProductDesignsData = $request->input('orderProducts.*.designs');

        // Map each design in the request to the appropriate format for OrderProducts->product_designs
        // Also, add each design via the XML API (this part needs to succeed before we try to create an order)
        $client = new GuzzleClient();
        $inDev = config('constants.IN_DEVELOPMENT');
        $orderProductDesigns = collect($request->input('orderProducts.*.designs'))
            ->map(function($designData) use (&$client, $inDev) {
                return collect($designData)->map(function($designData) use (&$client, $inDev) {
                    $dataMappedFromRequest = keys_to_snake(
                        keys_to_map(
                            (object) array_only((array) $designData, [
                                'printroId',
                                'designUrl',
                                'designName',
                                'placement',
                                'width',
                                'height',
                                'colors',
                                'fields',
                                'data'
                            ]), [
                                'colors' => 'color',
                                'fields' => 'field',
                            ]
                        )
                    );

                    // Map printro_id values to color_id values
                    $dataMappedFromRequest->color = collect($dataMappedFromRequest->color)->map(function($printro_id) {
                        return Color::fromPrintroId($printro_id)->first()->gd_id;
                    })->toArray();

                    // If in "dev" environment, skip API call and just get a random existing design
                    if ($inDev) {
                        $design = Design::inRandomOrder()->first();
                        $designVersion = $design->designVersions()->inRandomOrder()->first();
                        return (object) ([
                            'id' => $design->id ?? null,
                            'design_size_id' => $designVersion->id ?? null,
                        ] + (array) $dataMappedFromRequest);
                    }

                    $renderAttributeData = array_only((array) $dataMappedFromRequest, [ 'color', 'field', 'data' ]);
                    $renderAttributeCollection = collect($renderAttributeData)->map(function($array, $key) {
                        $type = ucfirst($key);
                        return collect($array)->map(function($value, $key) use ($type) {
                            return [
                                'type' => $type,
                                'key' => $key,
                                'value' => $value,
                            ];
                        })->all();
                    })->flatten(1);

                    $renderAttributeXMLCollection = $renderAttributeCollection->map(function($renderAttribute) {
                        [ 'type' => $type, 'key' => $key, 'value' => $value ] = $renderAttribute;
                        return <<<HEREDOC
                            <RenderAttribute>
                                <Type>$type</Type>
                                <Key>$key</Key>
                                <Value>$value</Value>
                            </RenderAttribute>
                            HEREDOC;
                    });

                    $renderAttributesXML = implode("\r\n", $renderAttributeXMLCollection->all()); // Collection of attribute XML to a single string
                    $renderAttributesXML = ltrim(preg_replace('/^/m', '            ', $renderAttributesXML)); // Indent to match placement in $body string

                    $fileExtension = pathinfo($designData['designUrl'], PATHINFO_EXTENSION);
                    $blobStoragePattern = '#^' . preg_quote('https://printroprod.blob.core.windows.net') . '#';
                    $appendedDesignUrl = htmlspecialchars($designData['designUrl'] . (
                        preg_match($blobStoragePattern, $designData['designUrl'])
                        ? '?sv=2018-03-28&ss=bf&srt=co&sp=rl&st=2019-01-21T19%3A42%3A41Z&se=2024-01-22T19%3A42%3A00Z&sig=dOizSEk%2Bq9E3T%2FTGpnmOa3LaaqOv%2FUp1o%2BbqZrAEOCA%3D'
                        : ''
                    ));
                    $body = <<<HEREDOC
                        <?xml version="1.0"?>
                        <!-- WRAPPER REQUEST TO AUTHORIZE API -->
                        <AuthorizationRequest Key="ec590bcdeeec3761b467140c62df8ce5">
                            <AddDigitalAssetRequest>
                                <StoreID>9</StoreID>
                                <DigitalAsset>
                                    <NotPrintable>TRUE</NotPrintable>
                                    <Name>{$designData['designName']}</Name>
                                    <Width>{$designData['width']}</Width>
                                    <Height>{$designData['height']}</Height>
                                    <FileURL>{$appendedDesignUrl}</FileURL>
                                    <FileExtension>{$fileExtension}</FileExtension>
                                    <PrintroID>{$designData['printroId']}</PrintroID>
                                    {$renderAttributesXML}
                                </DigitalAsset>
                            </AddDigitalAssetRequest>
                        </AuthorizationRequest>
                        HEREDOC;

                    $response = $client->post('http://api.greendistro.com/api/xml/xml/', [ 'body' => $body ]);
                    $xml = simplexml_load_string((string) $response->getBody());
                    $responseData = $xml->RequestResponse->Response;

                    return [
                        'id' => (integer) $responseData->DesignID,
                        'design_size_id' => (integer) $responseData->DesignSizeID,
                    ] + (array) $dataMappedFromRequest;
                })->all();
            })->all();
        unset($client);

        // Might as well map services data here, too
        $orderProductServices = collect($request->input('orderProducts.*.services'))
            ->map(function($serviceData) use ($customerId) {
                return collect($serviceData)->map(function($serviceData) use ($customerId) {
                    $service = Service::findOrFail($serviceData['serviceId']);

                    $prices = $service->getPrices($customerId, $serviceData['quantity']);
                    return [
                        'id' => $service->id,
                        'quantity' => $serviceData['quantity'],
                        'price' => $prices->unit,
                        'total' => $prices->total,
                        'unit_price' => $prices->unit,
                        'total_price' => $prices->total,
                    ] + (isset($serviceData['serviceData']) ? [
                        'data' => $serviceData['serviceData'],
                    ] : []);
                })->all();
            })->all();

        // Create Order
        $order = new Order([
            'user_id' => $request->input('userId'),
            'user_order_number' => $request->input('poNumber'),
            'bill_to_id' => $request->input('billingAddressId'),
            'ship_to_id' => $request->input('shippingAddressId'),
            'comments' => $request->input('comments'),
            'ship_method' => ShippingMethod::findByFullName($request->input('shippingMethod'))->method_code,
        ]);

        // NOTE: Must have all valid Products before we save the Order
        $orderProducts = collect($request->input('orderProducts'))
            ->map(function($data, $index) use ($orderProductDesigns, $orderProductServices) {
                $productData = array_only((array) $data, [ 'formula', 'width', 'height', 'numberOfColors' ]);
                $product = Product::matches($productData)->findLowestPriceOrFail($data['quantity']);
                return new OrderProduct([
                    'product_id' => $product->id,
                    'product_designs' => $orderProductDesigns[$index],
                    'product_services' => $orderProductServices[$index],
                    'product_data' => $data['data'],
                    'product_quantity' => $data['quantity'],
                ]);
            });

        // Assign OrderProducts to the new order
        $order->saveWithOrderProducts($orderProducts);

        // Grab an OrderIdentifierResource based on the created Order model
        $resource = new OrderIdentifierResource($order);
        // $resource = $order->toArray() + [ 'orderProducts ' => $order->orderProducts ];

        // Send OrderIdentifierResource resource based on the created Order model
        return $resource;
    }

    /**
     * Show the order that matches up with the given orderId value.
     *
     * @param  \App\Http\Requests\Printro\OrderReadRequest  $request
     * @return \App\Http\Resources\Printro\OrderResource
     */
    public function show(OrderReadRequest $request)
    {
        $order = Order::with([
            'orderProducts.product',
            'billingAddress',
            'shippingAddress',
            'shipments.packages',
        ])->findOrFail($request->input('orderId'));

        return new OrderResource($order);
    }

    /**
     * Display open order list for provided user.
     *
     * @param  \App\Http\Requests\Printro\UserOrderListReadRequest  $request
     * @return \App\Http\Resources\Printro\OrderCollection
     */
    public function showOpen(UserOrderListReadRequest $request)
    {
        return $this->showOrderList($request, true);
    }

    /**
     * Display closed order list for provided user.
     *
     * @param  \App\Http\Requests\Printro\UserOrderListReadRequest  $request
     * @return \App\Http\Resources\Printro\OrderCollection
     */
    public function showClosed(UserOrderListReadRequest $request)
    {
        return $this->showOrderList($request, false);
    }

    protected function showOrderList(UserOrderListReadRequest $request, $isOpenOrder = false)
    {
        $related = [ 'orderProducts:id,product_quantity,unit_tax,unit_price,total_price', 'shipments.packages' ];
        $perPage = $request->input('perPage');
        $customerId = $request->input('userId');

        $orders = Order::statusGroup($isOpenOrder ? StatusTree::OPEN : StatusTree::CLOSED)
            ->where('user_id', $customerId)
            ->with($related)
            ->paginate($perPage)
            ->appends($request->only([ 'perPage', 'userId' ]));

        return (new OrderSummaryCollection($orders))->toArray($request, $isOpenOrder);
    }
}
