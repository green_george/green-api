<?php

namespace App\Http\Controllers\Printro;

use App\Http\Controllers\Controller;
use App\Http\Requests\Printro\UserAddressListReadRequest;
use App\Http\Resources\Printro\AddressCollection;
use App\Address;

class AddressController extends Controller
{
    /**
     * Display address list for provided user.
     *
     * @param  \App\Http\Requests\Printro\UserAddressListReadRequest  $request
     * @return \App\Http\Resources\Printro\AddressCollection
     */
    public function showShipping(UserAddressListReadRequest $request)
    {
        $customerId = $request->input('userId');

        $addresses = Address::query()
            ->where('user_id', $customerId)
            ->where('address_type', Address::SHIPPING_ADDRESS_TYPE)
            ->get();

        return new AddressCollection($addresses);
    }

    /**
     * Display address list for provided user.
     *
     * @param  \App\Http\Requests\Printro\UserAddressListReadRequest  $request
     * @return \App\Http\Resources\Printro\AddressCollection
     */
    public function showBilling(UserAddressListReadRequest $request)
    {
        $customerId = $request->input('userId');

        $addresses = Address::query()
            ->where('user_id', $customerId)
            ->where('address_type', Address::BILLING_ADDRESS_TYPE)
            ->get()->map(function ($address) {
                $updatedAddress = array_merge($address->toArray(), [
                    'paymentMethod' => $address->payment_method_type,
                    'paymentMethodDescription' => $address->payment_method_description,
                ] + $address->getAuthorizeNetCreditCardDetails());
                return $updatedAddress;
            });

        return new AddressCollection($addresses);
    }
}
