<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Collect query parameters
        $include = !empty($request->input('include')) ? explode(',', $request->input('include')) : [];

        // TODO: Merge this with include - explode(',', $fields[$resourceType])
        // $fields = $request->input('fields') ?: [];

        // TODO: Sorting: -column_name = `column_name` DESC, column_name = `column_name` ASC

        // Get data
        $query = Order::query();
        if (count($include) > 0) $query = $query->with($include);

        // Return response
        return new OrderCollection($query->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $include = !empty($request->input('include')) ? explode(',', $request->input('include')) : [];

        $query = Order::query();
        if (count($include) > 0) $query = $query->with($include);
        $order = $query->findOrFail($id);

        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
