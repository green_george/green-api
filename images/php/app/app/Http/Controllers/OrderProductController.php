<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderProductCollection;
use App\Http\Resources\OrderProductResource;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;

class OrderProductController extends Controller
{

    /**
     * Display a listing of the resource.
     * @param  int  $orderId
     *
     * @return \Illuminate\Http\Response
     */
    public function index($orderId)
    {
        return new OrderProductCollection(
            Order::findOrFail($orderId)->orderProducts
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return new OrderProductResource(
            OrderProduct::findOrFail($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
