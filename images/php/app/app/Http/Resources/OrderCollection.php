<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\OrderResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($orderProduct) use ($request) {
            return (new OrderResource($orderProduct))->toArray($request);
        });
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        $hasOrderProducts = count($this->collection) > 0 && $this->collection->first()->relationLoaded('orderProducts');
        if (!$hasOrderProducts) return [];

        return [
            'included' => $this->collection->pluck('orderProducts')->flatten(1)->unique()->values()->map(function ($orderProduct) {
                return new OrderProductResource($orderProduct);
           })
        ];
    }
}
