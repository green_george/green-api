<?php

namespace App\Http\Resources\Printro;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return keys_to_map(parent::toArray($request), [
            'type' => 'addressType',
            'cim' => 'addressCim',
            'mis' => 'addressMis',
        ]);
    }
}
