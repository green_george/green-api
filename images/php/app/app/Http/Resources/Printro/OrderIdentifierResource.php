<?php

namespace App\Http\Resources\Printro;

class OrderIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return [ 'orderId' => $this->id ];
    }
}
