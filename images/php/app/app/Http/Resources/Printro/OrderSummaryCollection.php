<?php

namespace App\Http\Resources\Printro;

class OrderSummaryCollection extends JsonCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\Printro\OrderSummaryResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->resource->getCollection()->transform(function ($orderSummary) use ($request) {
            return (new OrderSummaryResource($orderSummary));
        });

        // Return the collection but convert the possible snake case pagination props to camel casing
        return collect($this->resource->toArray())->mapWithKeys(function ($value, $key) {
            return [ camel_case($key) => $value ];
        });
    }
}
