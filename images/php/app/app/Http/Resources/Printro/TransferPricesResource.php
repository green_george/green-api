<?php

namespace App\Http\Resources\Printro;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferPricesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return [
            'unitPrice' => round($this->unit_price, 2),
            'servicePrice' => round($this->service_price, 2),
            'totalPrice' => round($this->total_price, 2),
            'productId' => $this->product_id,
            'productName' => $this->product_name,
        ];
    }
}
