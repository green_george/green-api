<?php

namespace App\Http\Resources\Printro;

use Illuminate\Http\Resources\Json\ResourceCollection;

class JsonCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\JsonResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return parent::toArray($request);
    }
}
