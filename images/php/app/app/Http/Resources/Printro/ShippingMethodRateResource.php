<?php

namespace App\Http\Resources\Printro;

class ShippingMethodRateResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return [
            'method' => $this->method,
            'price' => $this->price,
            'estimatedDelivery' => $this->estimated_delivery,
        ];
    }
}
