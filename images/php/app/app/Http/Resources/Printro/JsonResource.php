<?php

namespace App\Http\Resources\Printro;

use Illuminate\Http\Resources\Json\JsonResource as IlluminateJsonResource;

class JsonResource extends IlluminateJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // If resource is a primitive data type, return it as-is
        if (!is_array($this->resource) && !is_object($this->resource))
            return $this->resource;

        // Otherwise, process it as an array
        $attrs = is_array($this->resource)
            ? $this->resource
            : $this->resource->toArray();

        // Map the array to a new array with camel case keys
        return keys_to_camel($attrs);
    }
}
