<?php

namespace App\Http\Resources\Printro;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferOrderPricesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();
        return [
            'subtotal' => $this->subtotal,
            'shippingPrice' => $this->ship_price,
            'tax' => $this->tax,
            'total' => $this->total,
        ];
    }
}
