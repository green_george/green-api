<?php

namespace App\Http\Resources\Printro;

class AddressCollection extends JsonCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\Printro\AddressResource';
}
