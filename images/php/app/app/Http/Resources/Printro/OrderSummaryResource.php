<?php

namespace App\Http\Resources\Printro;

use App\Other\StatusTree;

class OrderSummaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();

        $dateFormat = config('constants.PRINTRO_DATE_FORMAT');
        $dateProp = $this->status_group2 === StatusTree::OPEN ? 'estimatedDeliveryDate' : 'deliveryDate';

        $prices = $this->getPrices();

        $orderResponse = [
            'orderId' => $this->id,
            'poNumber' => $this->user_order_number,
            'orderDate' => $this->time_created->format($dateFormat),
            'status' => $this->status_group1,
            'subtotalPrice' => round($prices['subtotal'], 2),
            'tax' => round($prices['tax'], 2),
            'totalPrice' => round($prices['total'], 2),
            'packages' => $this->packages->map(function($package) use ($dateFormat, $dateProp) {
                return [ $dateProp => $package[$dateProp]->format($dateFormat) ] + $package;
            }),
        ];

        return $orderResponse;
    }
}
