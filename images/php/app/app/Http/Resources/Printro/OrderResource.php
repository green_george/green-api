<?php

namespace App\Http\Resources\Printro;

use Carbon\Carbon;
use App\ShippingMethod;
use App\Color;
use App\Service;
use App\Design;
use App\Other\StatusTree;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->withoutWrapping();

        $dateFormat = config('constants.PRINTRO_DATE_FORMAT');
        $dateProp = $this->status_group2 === StatusTree::OPEN ? 'estimatedDeliveryDate' : 'deliveryDate';

        // Get the most recent package
        $mostRecentPackage = $this->packages->reduce(function($prevPackage, $package) use ($dateProp) {
            $isNewer = $prevPackage[$dateProp] === null || $prevPackage[$dateProp]->greaterThanOrEqualTo($package[$dateProp]);
            return $isNewer ? $package : $prevPackage;
        });

        $prices = $this->getPrices();

        // NOTE: Didn't use a Relation since it wouldn't always match up
        $shippingMethod = ShippingMethod::where('method_code', $this->ship_method)->first();

        return [
            'orderId'         => $this->id,
            'poNumber'        => $this->user_order_number,
            'orderStatus'     => $this->status_group1,
            'orderDate'       => $this->time_created !== null ? $this->time_created->format($dateFormat) : null,
            $dateProp         => $mostRecentPackage !== null ? $mostRecentPackage[$dateProp]->format($dateFormat) : null,
            'shipDate'        => $this->getOriginal('time_donotshipbefore') !== -1 ? $this->time_donotshipbefore->format($dateFormat) : null,
            'shippingMethod'  => $shippingMethod->full_name ?? $this->ship_method,
            'shippingAddress' => (new AddressResource($this->shippingAddress))->toArray($request),
            'billingAddress'  => (new AddressResource($this->billingAddress))->toArray($request),
            'packages'        => $this->packages->map(function($package) use ($dateProp) {
                return array_except((array) $package, [ 'shippingMethod', $dateProp ]);
            }),
            'orderProducts'   => $this->orderProducts->map(function($orderProduct) {
                $servicesData =
                    // Optional services
                    $orderProduct->product_services->map(function($serviceData) {
                        return [
                            'serviceId'   => $serviceData->id,
                            'serviceName' => Service::find($serviceData->id)->service_desc,
                            'quantity'    => $serviceData->quantity ?? 1,
                            'unitPrice'   => $serviceData->unit_price ?? null,
                            'totalPrice'  => $serviceData->total_price ?? null,
                        ] + (isset($serviceData->data) ? [
                            'serviceData' => keys_to_camel($serviceData->data),
                        ] : []);
                    })

                    // Merge with the required services for the given product
                    ->merge($orderProduct->product->services(true)->get()->map(function($service) {
                        return [
                            'serviceId'   => $service->id,
                            'serviceName' => $service->service_desc,
                            'quantity'    => 1,
                            'unitPrice'   => $service->service_price ?? null,
                            'totalPrice'  => $service->service_price ?? null,
                        ];
                    }));

                return [
                    'productId'      => $orderProduct->product_id,
                    'productSku'     => $orderProduct->product->product_sku,
                    'productName'    => $orderProduct->product->product_name,
                    'productFormula' => $orderProduct->product->meta->{'@attribute:formula'},
                    'quantity'       => $orderProduct->product_quantity,
                    'unitPrice'      => $orderProduct->unit_price,
                    'totalPrice'     => $orderProduct->total_price,
                    'services'       => $servicesData,
                    'designs'        => $orderProduct->product_designs->map(function($designData) {

                        // Get specific design info for the response
                        $design = (object) (array_except((array) $designData, [
                            'rotation',
                            'print_ready_file',
                            'print_ready_file_name',
                            'hot_folder',
                            'pace_job',
                            'pace_job_part',
                            'pace_run',
                        ]) + [
                            'design_name' => Design::find($designData->id)->design_name,
                        ]);

                        $design->color = (object) collect($design->color)->map(function($gd_id) {
                            return Color::where('gd_id', $gd_id)->first()->printro_id;
                        })->toArray();

                        // Re-map array keys
                        return keys_to_camel(
                            keys_to_map($design, [
                                'id' => 'design_id',
                                'color' => 'colors',
                                'field' => 'fields',
                            ])
                        );
                    }),
                    'data'           => keys_to_camel(
                        keys_to_map($orderProduct->product_data, [
                            'attribute' => 'attributes',
                        ])
                    ),
                ];
            }),
            'shippingPrice'   => round($prices['ship_price'], 2),
            'subtotal'        => round($prices['subtotal'], 2),
            'tax'             => round($prices['tax'], 2),
            'total'           => round($prices['total'], 2),
        ];
    }
}
