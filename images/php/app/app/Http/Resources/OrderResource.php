<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

class OrderResource extends JsonResource
{
    private function getCollection($collectionName) {
        return $this->whenLoaded($collectionName);
    }

    private function getResourceIdentifiers($collectionName, $resourceClass, $request) {
        $collection = $this->getCollection($collectionName);
        if ($collection instanceof MissingValue) return null;

        return $collection->map(function ($resource) use ($resourceClass, $request) {
            $arr = (new $resourceClass($resource))->toArray($request);
            return [
                'type' => $arr['type'],
                'id' => (string) $arr['id'],
            ];
        });
    }

    private function getResources($collectionName, $resourceClass) {
        $collection = $this->getCollection($collectionName);
        if ($collection instanceof MissingValue) return null;

        return $collection->unique()->values()->map(function ($resource) use ($resourceClass) {
            return new $resourceClass($resource);
        });
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $orderProducts = $this->getResourceIdentifiers('orderProducts', OrderProductResource::class, $request);

        return [
            'type' => 'orders',
            'id' => $this->id,
            'attributes' => [
                'internalOrderNumber' => $this->internal_order_number,
                'integrationOrderId'  => $this->integration_order_id,
                'comments' => $this->comments,
            ],
            $this->mergeWhen($orderProducts !== null, [
                'relationships' => [
                    'orderProducts' => $orderProducts,
                ],
            ])
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        $orderProducts = $this->getResources('orderProducts', OrderProductResource::class);
        // dd(print_r($orderProducts, 1));
        if ($orderProducts === null) return [];

        return [
            'included' => $orderProducts,
        ];
    }
}
