<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'orderProducts',
            'id' => $this->id,
            'attributes' => [
                'productDesigns' => $this->product_designs,
            ],
        ];
    }
}
