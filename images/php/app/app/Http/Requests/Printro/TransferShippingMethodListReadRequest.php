<?php

namespace App\Http\Requests\Printro;

use App\Http\Requests\Request;

class TransferShippingMethodListReadRequest extends Request
{
    /**
     * Defaults to apply when values are missing (before validation).
     *
     * @return array
     */
    public function defaults()
    {
        return [
            'products.*.numberOfColors' => 0,
            'products.*.quantity' => 1,
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required|numeric|min:0|exists:wm_users,id',
            'addressId' => 'required|numeric|min:0|exists:wm_addresses,id',
            'products' => 'required', // Requires at least one product in an array
            'products.*.formula' => 'required|string|min:1',
            'products.*.numberOfColors' => 'required|numeric|min:0|color_pairs_with_formula',
            'products.*.width' => 'required|numeric|min:0',
            'products.*.height' => 'required|numeric|min:0',
            'products.*.quantity' => 'required|numeric|min:1',
        ];
    }
}
