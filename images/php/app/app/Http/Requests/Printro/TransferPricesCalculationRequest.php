<?php

namespace App\Http\Requests\Printro;

use App\Http\Requests\Request;

class TransferPricesCalculationRequest extends Request
{
    public $matchingProduct = null;

    /**
     * Defaults to apply when values are missing (before validation).
     *
     * @return array
     */
    public function defaults()
    {
        return [
            'numberOfColors' => 0,
            'quantity' => 1,
            'services.*.quantity' => 1,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId'         => 'numeric|min:0|exists:wm_users,id',
            'formula'        => 'required|string|min:1',
            'numberOfColors' => 'required|numeric|min:0|color_pairs_with_formula',
            'width'          => 'required|numeric|min:0',
            'height'         => 'required|numeric|min:0',
            'quantity'       => 'required|numeric|min:1',
            'services.*.serviceId' => 'required|numeric|min:0|exists:wm_services,id',
            'services.*.quantity'  => 'required|numeric|min:1',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $failed = $validator->failed();

            if (
                isset($failed["formula"]) || isset($failed["numberOfColors"]) ||
                isset($failed["width"]) || isset($failed["height"])
            ) return;

            // Get a matching product or fail
            $this->matchingProduct = \App\Product::with(['meta:_minimum_quantity'])->matches(
                $this->only([ 'formula', 'numberOfColors', 'width', 'height' ])
            )->findLowestPriceOrFail($this->input('quantity'));

            // If we succeeded in getting a product, ensure the product quantity is at or above the minimum
            $minimumQuantity = (int) $this->matchingProduct->meta->_minimum_quantity;
            $attribute = 'quantity';
            if ((int) $this->input($attribute) < $minimumQuantity) {
                $validator->errors()->add($attribute, "1 - The $attribute must be greater than or equal to $minimumQuantity - the product minimum value.");
            }

        });
    }
}
