<?php

namespace App\Http\Requests\Printro;

use App\Http\Requests\Request;

class TransferOrderCreateRequest extends Request
{
    /**
     * Defaults to apply when values are missing (before validation).
     *
     * @return array
     */
    public function defaults()
    {
        return [
            'poNumber' => '-99',
            'comments' => '',
            'orderProducts.*.services' => [],
            'orderProducts.*.services.*.quantity' => 1,
            'orderProducts.*.designs.*.placement' => 'center',
            'orderProducts.*.designs.*.colors' => [],
            'orderProducts.*.designs.*.fields' => [],
            'orderProducts.*.designs.*.data' => [],
            'orderProducts.*.data' => [],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required|numeric|min:0|exists:wm_users,id',
            'poNumber' => 'required|min:1|max:50',
            'billingAddressId' => 'required|numeric|min:0|exists:wm_addresses,id',
            'shippingAddressId' => 'required|numeric|min:0|exists:wm_addresses,id',
            'shippingMethod' => 'required|string|min:1',
            'orderProducts' => 'required',
            'orderProducts.*.formula' => 'required|string|min:0',
            'orderProducts.*.numberOfColors' => 'required|numeric|min:0', // 0 = fullcolor
            'orderProducts.*.width' => 'required|numeric|min:0',
            'orderProducts.*.height' => 'required|numeric|min:0',
            'orderProducts.*.quantity' => 'required|numeric|min:0',
            'orderProducts.*.services.*.serviceId' => 'required|numeric|min:0|exists:wm_services,id',
            'orderProducts.*.services.*.quantity' => 'required|numeric|min:1',
            'orderProducts.*.designs' => 'required',
            'orderProducts.*.designs.*.printroId' => 'required',
            'orderProducts.*.designs.*.designUrl' => 'required',
            'orderProducts.*.designs.*.designName' => 'required',
            'orderProducts.*.designs.*.placement' => 'required',
            'orderProducts.*.designs.*.width' => 'required|numeric|min:0',
            'orderProducts.*.designs.*.height' => 'required|numeric|min:0',
        ];
    }
}
