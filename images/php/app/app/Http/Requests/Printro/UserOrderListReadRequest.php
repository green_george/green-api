<?php

namespace App\Http\Requests\Printro;

use App\Http\Requests\Request;

class UserOrderListReadRequest extends Request
{
    public function defaults()
    {
        return [
            'perPage' => 15,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required|numeric|min:0|exists:wm_users,id'
        ];
    }
}
