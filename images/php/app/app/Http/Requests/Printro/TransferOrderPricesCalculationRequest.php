<?php

namespace App\Http\Requests\Printro;

use App\Http\Requests\Request;
use Illuminate\Support\Arr;

class TransferOrderPricesCalculationRequest extends Request
{
    public $matchingProducts = [];

    /**
     * Defaults to apply when values are missing (before validation).
     *
     * @return array
     */
    public function defaults()
    {
        return [
            'orderProducts.*.numberOfColors' => 0,
            'orderProducts.*.quantity' => 1,
            'orderProducts.*.services.*.quantity' => 1,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required|numeric|min:0|exists:wm_users,id',
            'shippingAddressId' => 'required|numeric|min:0|exists:wm_addresses,id',
            'shippingMethod' => 'required|string|min:0',
            'orderProducts' => 'required',
            'orderProducts.*.formula' => 'required|string|min:1',
            'orderProducts.*.numberOfColors' => 'required|numeric|min:0|color_pairs_with_formula',
            'orderProducts.*.width' => 'required|numeric|min:0',
            'orderProducts.*.height' => 'required|numeric|min:0',
            'orderProducts.*.quantity' => 'required|numeric|min:1',
            'orderProducts.*.services.*.serviceId' => 'required|numeric|min:0|exists:wm_services,id',
            'orderProducts.*.services.*.quantity' => 'required|numeric|min:1',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $failed = $validator->failed();
            $orderProducts = $this->input('orderProducts');

            // Get matching products or fail
            $this->matchingProducts = collect($orderProducts)->map(function($data, $key) use ($failed) {
                $parentKey = "orderProducts.$key";
                $failedValidation =
                    isset($failed["$parentKey.formula"])
                    || isset($failed["$parentKey.numberOfColors"])
                    || isset($failed["$parentKey.width"])
                    || isset($failed["$parentKey.height"])
                ;

                return $data + [
                    'product' => !$failedValidation
                        ? \App\Product::with(['meta:_minimum_quantity'])->matches($data)->findLowestPriceOrFail($data['quantity'])
                        : null,
                ];
            })->all();

            // If we succeeded in getting products, ensure each product quantity is at or above the minimum
            foreach($orderProducts as $index => $orderProduct) {
                $product = $this->matchingProducts[$index]['product'];
                if (!$product) continue;
                $minimumQuantity = $product->meta->_minimum_quantity;

                if ($orderProduct['quantity'] < $minimumQuantity) {
                    $attribute = "orderProducts.$index.quantity";
                    $validator->errors()->add($attribute, "1 - The $attribute must be greater than or equal to $minimumQuantity - the product minimum value.");
                }
            }
        });
    }
}
