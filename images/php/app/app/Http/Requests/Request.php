<?php

namespace App\Http\Requests;

use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Validation\ValidationException;
use \Illuminate\Validation\DatabasePresenceVerifier;

abstract class Request extends IlluminateRequest
{
    public function validate()
    {
        // Replace/remove "required" and "present" rules if they have a default value
        $rules = self::rulesAfterDefaults($this->rules(), $this->defaults());

        $validator = app('validator')->make($this->all(), $rules, $this->messages());

        // Set the database connection to be used by default for validation
        $databaseManager = clone app('db');
        $databaseManager->setDefaultConnection('wm_dashboard');
        $presenceVerifier = new DatabasePresenceVerifier($databaseManager);
        $validator->setPresenceVerifier($presenceVerifier);

        // Enables child Request classes to further extend the validation logic
        $this->withValidator($validator);

        $validator->validate();
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        //
    }

    /**
     * Defaults to apply when values are missing (before validation).
     *
     * @return array
     */
    public function defaults()
    {
        return [];
    }

    /**
     * Sets the parameters for this request.
     *
     * This method also re-initializes all properties.
     *
     * @param array                $query      The GET parameters
     * @param array                $request    The POST parameters
     * @param array                $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array                $cookies    The COOKIE parameters
     * @param array                $files      The FILES parameters
     * @param array                $server     The SERVER parameters
     * @param string|resource|null $content    The raw body data
     */
    public function initialize(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::initialize($query, $request, $attributes, $cookies, $files, $server, $content);

        // Add defaults into input data (using dot notation)
        $data = $this->all();
        foreach($this->defaults() as $key => $value) {
            data_fill($data, $key, $value);
        }
        $this->replace($data);
    }

    protected static function rulesAfterDefaults($rules, $defaults)
    {
        return array_map_with_keys($rules, function($rule, $key) use ($defaults) {

            // If there's no default value...make no changes
            if (!isset($defaults[$key])) return [ $key => $rule ];

            // Ensure we are always dealing with an array
            $wasArray = is_array($rule);
            $ruleArray = $wasArray ? $rule : explode('|', $rule);

            // Remove all "present" rules and replace "required" rules with "filled"
            // NOTE: "present" doesn't make sense for something with a default value - it will always be present
            // Also, "required" = "filled" + "present"
            $ruleArray = array_remove_value($ruleArray, 'present');
            $ruleArray = str_replace('required', 'filled', $ruleArray);

            return [ $key =>
                $wasArray ? $ruleArray : implode('|', $ruleArray) // convert back to string
            ];
        }, []);
    }

    protected function rules()
    {
		return [];
	}

    protected function messages()
    {
        return [];
    }
}
