<?php

namespace App\Http\Middleware;

use Closure;

class JsonApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Accept') !== 'application/vnd.api+json')
            abort(415, 'Invalid "Accept" header. Set to "application/vnd.api+json"');
        return $next($request)->header('Content-Type', 'application/vnd.api+json');
    }
}
