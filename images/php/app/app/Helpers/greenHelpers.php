<?php

/*
    NOTE: These functions are specific to existing Green Applications solutions.
    Please prefix all function names with "green_" and only add what is absolutely necessary at the global level.
*/

if (! function_exists('green_array_flatten')) {
    function green_array_flatten($array, &$targetArray = [], $parentKey = '')
    {
        foreach((array) $array as $key => $value) {
            if (is_array($value)) {
                $targetArray += green_array_flatten($value, $targetArray, "$parentKey@$key:");
                continue;
            }
            $targetArray[$parentKey . $key] = $value;
        }

        return $targetArray;
    }
}

if (! function_exists('green_array_unflatten')) {
    function green_array_unflatten($array, $targetArray = [])
    {
        // For each key, start at the top-most level of $targetArray
        foreach ((array) $array as $key => $value) {
            $parentArr = &$targetArray;

            // If we have a nested key, navigate inward (and create the nested arrays if they don't exist)
            while (preg_match('/^@(.+?)\:(.+)$/', $key, $matches)) {
                $parentKey = $matches[1];

                // If no array exists, create a new array
                if (!array_key_exists($parentKey, $parentArr))
                    $parentArr[$parentKey] = [];

                // Navigate to the inner array for the next iteration
                $key = $matches[2]; // start next loop with $key = (whatever comes after "@parentKey:")
                $parentArr = &$parentArr[$parentKey]; // start next loop with $parentArr = (the nested array)
            }

            // Finally, set the value
            $parentArr[$key] = $value;
        }
        return $targetArray;
    }
}
