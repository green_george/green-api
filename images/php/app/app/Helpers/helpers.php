<?php

// NOTE: The functions in this file are globally available. As a result, these should always be generic utility functions.

if (! function_exists('array_remove_value')) {
    function array_remove_value(array $array, $value)
    {
        if (($key = array_search($value, $array)) !== false) {
            unset($array[$key]);
        }
        return $array;
    }
}

if (! function_exists('array_map_with_keys')) {
    function array_map_with_keys(array $array, callable $callback)
    {
        $newArray = [];
        foreach($array as $key => $value) {
            $returned = $callback($value, $key);
            $newArray = array_merge($newArray, $returned);
        }
        return $newArray;
    }
}

if (! function_exists('keys_to_map')) {
    function keys_to_map($arrayOrObject, $keyMap)
    {
        $array = array_map_with_keys((array) $arrayOrObject, function($value, $key) use ($keyMap) {
            return [ ($keyMap[$key] ?? $key) => $value ];
        });

        return is_object($arrayOrObject) ? (object) $array : $array;
    }
}

if (! function_exists('keys_to_snake')) {
    function keys_to_snake($arrayOrObject)
    {
        $array = array_map_with_keys((array) $arrayOrObject, function($value, $key) {
            $value = is_array($value) ? keys_to_snake($value) : $value;
            return [ snake_case($key) => $value ];
        });

        return is_object($arrayOrObject) ? (object) $array : $array;
    }
}

if (! function_exists('keys_to_camel')) {
    function keys_to_camel($arrayOrObject)
    {
        $array = array_map_with_keys((array) $arrayOrObject, function($value, $key) {
            $value = is_array($value) ? keys_to_camel($value) : $value;
            return [ camel_case($key) => $value ];
        });

        return is_object($arrayOrObject) ? (object) $array : $array;
    }
}
