<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // return parent::render($request, $exception);

        $status = $this->getExceptionStatus($exception);

        $mainError = [
            'status' => $status,
            'detail' => $exception instanceof ModelNotFoundException
                ? class_basename($exception->getModel()) . ' not found.'
                : $exception->getMessage(),
        ];

        // Define $errors array
        // NOTE: If you'd like to add more information here, please see how it fits into the format:
        // https://jsonapi.org/format/#errors or https://jsonapi.org/examples/#error-objects-basics
        if ($exception instanceof ValidationException) {
            $errors = collect(
                $exception->validator->messages()->getMessages()
            )->flatMap(function($messages, $key) use ($status, $exception) {

                // Gather the failed attribute rule types
                $types = array_keys($exception->validator->failed()[$key] ?? []);

                // Build the error item for this message
                return array_map(function($message, $idx) use ($key, $status, $types) {
                    $hasCode = preg_match('/^(-?[0-9]+) - (.+)$/', $message, $matches);
                    $codeArray = $hasCode ? [ 'code' => $matches[1] ] : [];

                    return [
                        'status' => isset($types[$idx]) && $types[$idx] === 'Exists' ? 404 : $status,
                        'detail' => $hasCode ? $matches[2] : $message,
                        'source' => $key,
                    ] + $codeArray;
                }, $messages, array_keys($messages));
            })->toArray();

        } else {
            $errors = [ $mainError ];
        }

        // Find the minimum status code number out of all the errors
        $statusCode = min(array_map(function ($error) {
            return $error['status'];
        }, $errors));

        return response()
            ->json([
                'errors' => $errors,
            ], $statusCode)
            ->header('Content-Type', 'application/vnd.api+json');
    }

    /**
     * Get the status code that pairs with the given exception
     * @param \Exception $exception
     */
    protected function getExceptionStatus($exception)
    {
        if ($exception instanceof HttpException) {
            return $exception->getStatusCode();
        } elseif ($exception instanceof ModelNotFoundException) {
            return 404;
        } elseif ($exception instanceof AuthorizationException) {
            return 401;
        } elseif ($exception instanceof ValidationException) {
            return 400;
        }
        return 500;
    }
}
