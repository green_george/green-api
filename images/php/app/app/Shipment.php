<?php

namespace App;

class Shipment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_shipments';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;

    /**
     * Get the Order models related to this Shipment.
     */
    public function orders()
    {
        return static::storeIdOnPivot(
            $this->belongsToMany('App\Order', 'wm_order_shipments')
        );
    }

    /**
     * Get the Package models related to this Shipment.
     */
    public function packages()
    {
        return $this->hasMany('App\Package');
    }
}
