<?php

namespace App;

class Meta extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $foreignKey;
    protected $foreignId;
    protected $casts = [];

    public function setCasts($casts = [])
    {
        $this->casts = $casts;
        return $this;
    }

    public function save(array $options = [])
    {
        $baseQuery = \DB::connection($this->connection)->table($this->table);

        foreach ($this->getAttributes() as $k => $v) {
            // Check if this key exists in meta table. If so, use ->update(). Otherwise, use ->insert()
            $query = with(clone $baseQuery)->where('store_id', 9)->where($this->foreignKey, $this->foreignId)->where('meta_key', $k);
            if ($query->exists()) {
                $query->update([ 'meta_value' => $v ]);
            } else {
                $baseQuery->insert([ 'store_id' => 9, $this->foreignKey => $this->foreignId, 'meta_key' => $k, 'meta_value' => $v ]);
            }
        }
    }

    public function setForeignKey($foreignKey, $foreignId)
    {
        $this->foreignKey = $foreignKey;
        $this->foreignId = $foreignId;
        return $this;
    }
}
