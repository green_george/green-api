<?php

namespace App;

use App\Traits\Meta;

class Design extends Model
{
    use Meta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_designs';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'time_updated';

    /**
     * Create a new Eloquent Product model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        // Enable meta data to be searched and accessed
        $this->initMeta('wm_design_meta');
    }

    /**
     * Get the DesignVersion models related to this Design.
     */
    public function designVersions()
    {
        return $this->hasMany('App\DesignVersion');
    }
}
