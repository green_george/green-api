<?php

namespace App;

class DesignVersion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_design_sizes';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'time_updated';

    /**
     * Get the Design model related to this DesignVersion.
     */
    public function design()
    {
        return $this->belongsTo('App\Design');
    }
}
