<?php

namespace App;

class Package extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wm_packages';

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;

    protected $casts = [
        'package_freight' => 'float',
        'package_handling' => 'float',
    ];

    /**
     * Get the Shipment model related to this Package.
     */
    public function shipment()
    {
        return $this->belongsTo('App\Shipment');
    }

    /**
     * Get the ShippingMethod model related to this Package.
     */
    public function shippingMethod()
    {
        return $this->belongsTo('App\ShippingMethod', 'package_method', 'method_code');
    }

    /**
     * Get the tracking url (using ShippingMethod, if any).
     */
    public function getTrackingUrlAttribute()
    {
        $this->loadMissing('shippingMethod');
        if (!isset($this->shippingMethod) || empty($this->package_tracking)) return '';
        return $this->shippingMethod->tracking_url_prefix . $this->package_tracking;
    }

    /**
     * Get the ship price of the package
     */
    /*
    // NOTE: These values are not accurate yet, use ship_date on Order instead
    public function getShipPriceAttribute()
    {
        return $this->package_freight + $this->package_handling;
    }
    */
}
