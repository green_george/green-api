<?php
namespace App\Providers;

use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Support\ServiceProvider;
use App\Http\Requests\Request;

class RequestServiceProvider extends ServiceProvider
{
	public function register()
    {
        $this->registerRequests('Http/Requests');
        $this->registerRequests('Http/Requests/Printro');
	}

    protected function registerRequests($relativePath)
    {
        $requestPath = __DIR__ . '/../' . $relativePath;
        $files = glob("$requestPath/*.php");
        foreach ($files as $file) {
            $namespace = 'App\\'. str_replace ('/', '\\', $relativePath);
            $class = "$namespace\\" . basename($file, '.php');
            $this->app->bind($class, function ($app) use ($class) {
                $current = $app['request'];
                $files = $current->files->all();
                $files = is_array($files) ? array_filter($files) : $files;
                return new $class($current->query->all(), $current->request->all(), $current->attributes->all(), $current->cookies->all(), $files, $current->server->all(), $current->getContent());
            });

            $this->app->afterResolving($class, function ($request) {
                $request->validate();
            });
        }
    }

	public function boot()
    {
        //
	}
}
