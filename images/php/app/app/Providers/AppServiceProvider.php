<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot any application services.
     *
     * @return void
     */
    public function boot()
    {
        $validators = [
            'color_pairs_with_formula' => 'App\Validators\ColorPairsWithFormulaValidator',
        ];

        foreach($validators as $key => $class) {
            Validator::extend($key, "$class@validate");
            Validator::replacer($key, "$class@replaceInMessage");
        }

        \Shippo::setApiKey(env('SHIPPO_API_KEY'));

        // NOTE: Added this for debugging
        // dd(\Illuminate\Support\Facades\DB::getQueryLog()); will get all generated queries
        \DB::enableQueryLog();
    }
}
