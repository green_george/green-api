<?php

use Laravel\Lumen\Routing\Router;

function apiResource($router, $resourceName, $controllerName, $except = []) {
    $defaultMethods = [
        'index',
        'store',
        'show',
        'update',
        'destroy',
    ];

    $methods = array_filter($defaultMethods, function($method) use ($except) {
        return !in_array($method, $except);
    });

    $router->group([
        'prefix' => $resourceName,
    ], function (Router $router) use ($controllerName, $methods) {
        if (in_array('index', $methods)) $router->get('/', $controllerName . '@index');
        if (in_array('store', $methods)) $router->post('/', $controllerName . '@store');
        if (in_array('show', $methods)) $router->get('/{id:[\d]+}', $controllerName . '@show');
        if (in_array('update', $methods)) $router->put('/{id:[\d]+}', $controllerName . '@update');
        if (in_array('destroy', $methods)) $router->delete('/{id:[\d]+}', $controllerName . '@destroy');
    });
}

function apiResources(Router $router, array $resources = []) {
    foreach($resources as $resourceName => $controllerName) {
        apiResource($router, $resourceName, $controllerName);
    }
}
