<?php

use Laravel\Lumen\Routing\Router;

// NOTE: Printro-specific API routes (very basic, non-json:api stuff)
$router->group(['prefix' => 'printro/v1', 'middleware' => ['auth']], function (Router $router) {

    // Get Open Order List
    $router->get('openOrderList', 'Printro\OrderController@showOpen');

    // Get Closed Order List
    $router->get('closedOrderList', 'Printro\OrderController@showClosed');

    // Get Billing Addresses
    $router->get('billingAddresses', 'Printro\AddressController@showBilling');

    // Get Shipping Addresses
    $router->get('shippingAddresses', 'Printro\AddressController@showShipping');

    // Submit Transfer Order
    $router->post('transferOrder', 'Printro\OrderController@store');

    // Get Transfer Order Price
    $router->post('transferOrderPrices', 'Printro\OrderController@calculatePrices');

    // Get Order
    $router->get('printroOrder', 'Printro\OrderController@show');

    // Get Shipping Methods
    $router->post('shippingMethods', 'Printro\ShippingController@calculateShippingMethodRates');

    // Get Transfer Price
    $router->post('transferPrice', 'Printro\ProductController@calculateTransferPrices');
});
