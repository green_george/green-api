<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

require_once('helper.php');

// ----------------------------------------------------------------------------

$router->get('/', function () use ($router) {
    // return str_random(32); // Used this to temp generate a 32-char key for docker-compose.yml
    return $router->app->version();
});

// --------------------------------- Resources --------------------------------

// NOTE: Order matters here. First one to match will capture the request.
// Put more specific paths first and additional resource routes BEFORE apiResource calls
$router->group(['prefix' => 'v1', 'middleware' => ['auth', 'json']], function (Router $router) {
    $router->get('orders/{id:[\d]+}/orderProducts', 'OrderProductController@index');
    apiResource($router, 'orderProducts', 'OrderProductController', $except = [ 'index' ]);
    apiResource($router, 'orders', 'OrderController');
    apiResource($router, 'addresses', 'AddressController');
    apiResource($router, 'users', 'UserController');
});
