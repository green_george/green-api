<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class OauthUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_users')->insert([
            'name' => 'Test Account',
            'email' => 'testaccount@fmexpressions.com',
            'password' => Crypt::encrypt('Fmexp123'),
        ]);
    }
}
