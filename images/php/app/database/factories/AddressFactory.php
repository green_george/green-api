<?php

/*
 * Defines how a default App\Address model should look.
 */
$factory->define(App\Address::class, function (Faker\Generator $faker) {
    $fname = $faker->firstName;
    $lname = $faker->lastName;
    return [
        'address_type' => App\Address::SHIPPING_ADDRESS_TYPE,
        'address_cim' => -1,
        'address_mis' => '',
        'store_id' => 9,
        'company_name' => $faker->company,
        'shipping_name' => $fname . ' ' . $lname,
        'first_name' => $fname,
        'last_name' => $lname,
        'street1' => $faker->streetAddress,
        'street2' => $faker->secondaryAddress,
        'street3' => '',
        'city' => $faker->city,
        'region' => $faker->state,
        'zipcode' => $faker->postcode,
        'country' => $faker->country,
        'phone_number' => $faker->phoneNumber,
        'fax_number' => '',
        'email_address' => $faker->email,
        'time_created' => Carbon\Carbon::now()->timestamp,
        'is_default' => 0,
        'is_residential' => 0,
    ];
});
$factory->state(App\Address::class, 'validUS', [
    'street1' => '7214 Thatcher Dr.',
    'street2' => '',
    'street3' => '',
    'city' => 'Huntington Station',
    'region' => 'New York',
    'zipcode' => '11746',
    'country' => 'US',
]);
$factory->state(App\Address::class, 'shipping', [ 'address_type' => App\Address::SHIPPING_ADDRESS_TYPE ]);
$factory->state(App\Address::class, 'billing', [ 'address_type' => App\Address::BILLING_ADDRESS_TYPE ]);
