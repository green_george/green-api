<?php

/*
 * Defines how a default App\Customer model should look.
 */
$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'store_id' => 9,
        'store_balance' => '0.00',
        'parent_id' => -1,
        'user_rank_id' => 2,
        'user_status_id' => 1,
        'contact_id' => 747527,
        'default_ship_to_id' => 0,
        'default_bill_to_id' => 0,
        'default_forward_to_id' => -1,
        'recovery_question' => '',
        'recovery_answer' => '',
        'time_created' => 1454967852,
        'time_last' => 1521833158,
        'ip_last' => '72.90.182.78',
        'mis_code' => 'PrintStream',
        'mis' => 'TESTCUSN',
        'cim' => 214994279,
        'comments' => '',
        'temp' => '214994279',
        'availability' => 2,
        'email' => 'TESTACCOUNT@FMEXPRESSIONS.COM',
        'name' => 'TESTACCOUNT@FMEXPRESSIONS.COM',
        'password' => 'f629f8ffd7b0ecb461803007fa59871b'
    ];
});

$factory->afterCreating(App\Customer::class, function (App\Customer $customer) {
    $customer->shippingAddresses()->save(factory('App\Address')->states('validUS', 'shipping')->make());
    $customer->billingAddresses()->save(factory('App\Address')->states('billing')->make());
});
