<?php

$factory->define(App\PricingTable::class, function (Faker\Generator $faker) {
    $timestamp = $faker->unixTime();
    return [
        'store_id' => 9,
        'table_mode_id' => $faker->randomElement([2, 3]),
        'display_name' => "TEST-$timestamp-0",
        'time_created' => $timestamp,
        'last_updated' => $timestamp,
    ];
});

$factory->afterCreating(App\PricingTable::class, function (App\PricingTable $pricingTable, Faker\Generator $faker) {
    $pricingTable->tiers()->save(factory('App\PricingTableTier')->states('last')->make());
});
