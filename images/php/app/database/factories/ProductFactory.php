<?php

/*
 * Defines how a default App\Product model should look.
 */
$factory->define(App\Product::class, function (Faker\Generator $faker) {
    $sku = strtoupper($faker->bothify('TEST-??-##x##-??'));

    // Get valid dimensions at random
    $dimensions = $faker->randomElement([
        [ 0.01, 4.00, 4.00, 0.0300 ],
        [ 0.01, 12.00, 9.00, 0.0300 ],
        [ 0.01, 12.00, 12.00, 0.0300 ],
        [ 4.00, 4.00, 0.01, 0.0500 ],
        [ 4.00, 6.00, 0.01, 0.0044 ],
        [ 4.00, 6.00, 4.00, 0.0044 ],
        [ 4.00, 6.00, 4.00, 0.0220 ],
        [ 6.00, 6.00, 0.01, 0.0500 ],
        [ 6.00, 8.00, 0.01, 0.0500 ],
        [ 6.00, 9.00, 0.01, 0.0100 ],
        [ 6.00, 9.00, 6.00, 0.0100 ],
        [ 6.00, 9.00, 6.00, 0.0300 ],
        [ 8.00, 10.00, 0.01, 0.0500 ],
        [ 8.50, 11.00, 0.01, 0.0500 ],
        [ 9.00, 12.75, 0.01, 0.0200 ],
        [ 9.00, 12.75, 0.01, 0.0212 ],
        [ 9.00, 12.75, 0.01, 0.0500 ],
        [ 9.00, 12.75, 9.00, 0.0212 ],
        [ 12.00, 12.00, 0.01, 0.0212 ],
        [ 12.00, 12.00, 0.01, 0.0266 ],
        [ 12.00, 12.00, 0.01, 0.0500 ],
        [ 12.00, 12.00, 12.00, 0.0266 ],
        [ 12.00, 13.00, 0.01, 0.0500 ],
        [ 12.75, 19.00, 0.01, 0.0400 ],
        [ 12.75, 19.00, 0.01, 0.0513 ],
        [ 12.75, 19.00, 12.00, 0.0513 ],
        [ 12.75, 19.00, 12.00, 0.0648 ],
        [ 19.00, 25.50, 0.01, 0.0500 ],
    ]);
    list($width, $height, $length, $weight) = $dimensions;

    $timestamp = $faker->unixTime();

    return [
        'store_id' => 9,
        'method_id' => -1,
        'product_model_id' => 30, // 4, 5, 10, 25, or 30 all seem to be valid
        'product_type' => 1,
        'product_status' => 1,
        'product_stock_status' => 1,
        'product_sku' => $sku,
        'product_name' => 'Test Product Name',
        'product_desc' => 'Test Product Description',
        'product_html' => '',
        'product_length' => $length,
        'product_width' => $width,
        'product_height' => $height,
        'product_weight' => $weight,
        'product_tags' => '',
        'concurrent_placements' => 1,
        'is_taxable' => 0,
        'is_variant' => 0,
        'is_disabled' => 0,
        'is_public' => 1,
        'is_colormatch' => 0,
        'time_created' => $timestamp,
        'last_updated' => $timestamp,
    ];
});

$factory->afterMaking(App\Product::class, function (App\Product $product, Faker\Generator $faker) {
    // Get valid numberOfColors at random
    $numberOfColorsEnum = $faker->randomElement([
        '1color', '2color', '3color', '4color', '5color',
        '6color', '7color', 'freedom', 'foil', 'sublimate',
    ]);

    // Get valid formula at random ('sublimate' num of colors must match with 'sublimate' formula)
    $formulaEnum = $numberOfColorsEnum === 'sublimate' ? 'sublimate' : $faker->randomElement([
        'athletic', 'nylon', 'fashion', 'performance', 'apparel', 'foil',
        'vintage', 'polypropylene', 'flock', 'rhinestone',
    ]);

    $meta = $product->meta()->newRelatedInstance()->forceFill([
        '@attribute:ink' => $numberOfColorsEnum,
        '@attribute:formula' => $formulaEnum,

        // Get a valid minimum quantity at random
        '_minimum_quantity' => $faker->randomElement([
            1, 8, 10, 20, 24, 25, 40, 50, 72, 75, 100, 144, 200, 250, 512,
        ]),
    ]);

    $product->setRelation('meta', $meta);
});

// NOTE: Printro doesn't use "foil"
$factory->afterMakingState(App\Product::class, 'forPrintro', function (App\Product $product, Faker\Generator $faker) {
    // Get valid numberOfColors at random
    $numberOfColorsEnum = $faker->randomElement([
        '1color', '2color', '3color', '4color', '5color',
        '6color', '7color', 'freedom', 'sublimate',
    ]);

    // Get valid formula at random ('sublimate' num of colors must match with 'sublimate' formula)
    $formulaEnum = $numberOfColorsEnum === 'sublimate' ? 'sublimate' : $faker->randomElement([
        'athletic', 'nylon', 'fashion', 'performance', 'apparel',
        'vintage', 'polypropylene', 'flock', 'rhinestone',
    ]);

    $product->meta->forceFill([
        '@attribute:ink' => $numberOfColorsEnum,
        '@attribute:formula' => $formulaEnum,
    ]);
});

$factory->afterCreating(App\Product::class, function (App\Product $product, Faker\Generator $faker) {
    $product->meta()->save($product->meta);

    $pricingTable = factory('App\PricingTable')->create();
    $product->pricingTables()->attach($pricingTable->id, [
        'store_id' => 9,
        'time_created' => $faker->unixTime(),
    ]);

    $service = factory('App\Service')->create();
    $product->services()->attach($service->id, [
        'store_id' => 9,
        'is_required' => 1,
        'time_created' => $faker->unixTime(),
    ]);
});
