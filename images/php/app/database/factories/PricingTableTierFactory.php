<?php

$factory->define(App\PricingTableTier::class, function (Faker\Generator $faker) {
    $timestamp = $faker->unixTime();
    return [
        'store_id' => 9,
        'display_name' => "TEST-$timestamp-0",
        'first_quantity' => 0,
        'last_quantity' => 199,
        'price' => 5.00,
        'time_created' => $timestamp,
        'last_updated' => $timestamp,
    ];
});
$factory->state(App\PricingTableTier::class, 'last', [ 'last_quantity' => -1 ]);
