<?php

/*
 * Defines how a default App\User model should look.
 */
$factory->define(App\Service::class,
 function (Faker\Generator $faker) {
    return [
        'store_id' => 9,
        'service_desc' => 'TEST Service Fee',
        'service_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 200),
    ];
});

$factory->afterCreating(App\Service::class, function (App\Service $service, Faker\Generator $faker) {
    $pricingTable = factory('App\PricingTable')->create();
    $service->pricingTables()->attach($pricingTable->id, [
        'store_id' => 9,
        'time_created' => $faker->unixTime(),
        'is_inheiritable' => 0,
    ]);
});
