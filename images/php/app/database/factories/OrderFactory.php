<?php

/*
 * Defines how a default App\Order model should look.
 */
$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'store_id' => 9,
        'order_origin' => 'SYS',
        'order_type' => 4,
        'order_status' => 81,
        // 'user_id' => 31105, // NOTE: Must assign Order to a user before saving
        'user_order_number' => '719003',
        'internal_order_number' => 'S674388011',
        'secondary_order_number' => '',
        'integration_order_id' => '',
        'comments' => '',
        // 'ship_to_id' => 1273502, // NOTE: Must assign Order to a shipping address before saving
        // 'bill_to_id' => 1273501, // NOTE: Must assign Order to a billing address before saving
        'forward_to_id' => -1,
        'ship_method' => 'UPS_GND',
        'ship_price' => 20.00,
        'metric_order_total' => 0.00,
        'time_inhandsby' => 0,
        'time_cancelby' => -1,
        'time_donotshipbefore' => $faker->unixTime() + 604800, // 7 days from today
    ];
});
