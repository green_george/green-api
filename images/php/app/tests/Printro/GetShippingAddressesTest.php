<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\GetAddressesTestCase;

class GetShippingAddressesTest extends GetAddressesTestCase
{
    public function testGetsShippingAddresses()
    {
        $this->runAddressesTest(false);
    }

    public function testGetShippingAddressesFailsWhenNoUserIdIsProvided()
    {
        $this->runAddressesTest(false, self::MISSING_USER_ID_ERROR);
    }

    public function testGetShippingAddressesFailsWhenUserIdIsInvalid()
    {
        $this->runAddressesTest(false, self::INVALID_USER_ID_ERROR);
    }

    public function testGetShippingAddressesFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runAddressesTest(false, self::USER_NOT_FOUND_ERROR);
    }
}
