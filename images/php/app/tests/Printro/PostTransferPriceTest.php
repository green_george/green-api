<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\PrintroTestCase;
use App\Product;

class PostTransferPriceTest extends PrintroTestCase
{
    const MISSING_USER_ERROR = 1;
    const UNMATCHED_PRODUCT_ERROR = 2;
    const PRODUCT_MINIMUM_ERROR = 3;
    const MISSING_SERVICE_ERROR = 4;
    const NONZERO_NUM_OF_COLORS = 5;

    public function testCalculatesTransferPrice()
    {
        $this->runTransferPriceRequestTest();
    }

    public function testCalculateTransferPriceFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runTransferPriceRequestTest(self::MISSING_USER_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenServiceDoesNotExistWithProvidedServiceId()
    {
        $this->runTransferPriceRequestTest(self::MISSING_SERVICE_ERROR);
    }

    public function testCalculateTransferPriceFailsWhenNoMatchingProductIsFound()
    {
        $this->runTransferPriceRequestTest(self::UNMATCHED_PRODUCT_ERROR);
    }

    public function testCalculateTransferPriceFailsWhenBelowProductMinimumQuantity()
    {
        $this->runTransferPriceRequestTest(self::PRODUCT_MINIMUM_ERROR);
    }

    public function testCalculateTransferPriceFailsWhenSublimateFormulaIsUsedWithNonZeroNumberOfColor()
    {
        $this->runTransferPriceRequestTest(self::NONZERO_NUM_OF_COLORS);
    }

    protected function runTransferPriceRequestTest($errorType = 0)
    {
        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();
        $product = factory('App\Product')->states('forPrintro')->create();
        $service = factory('App\Service')->create();

        // Store info from models in case they get deleted in the next step
        $customerId = $customer->id;
        $serviceId = $service->id;
        $minimumQuantity = $product->meta->_minimum_quantity;
        $productData = [
            'formula' => $product->meta->{'@attribute:formula'},
            'width' => $product->width,
            'height' => $product->height,
            'numberOfColors' => $product->number_of_colors,
            'quantity' => $product->meta->_minimum_quantity,
            'services' => [
                [
                    'serviceId' => $serviceId,
                    'quantity' => 1,
                ],
            ],
        ];

        // Set up the situation
        Product::query()->matches(array_only($productData, [
            'formula', 'numberOfColors', 'width', 'height'
        ]))->where('id', '<>', $product->id)->delete(); // Delete all other matching products
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
                $customer->delete();
                break;
            case self::MISSING_SERVICE_ERROR:
                $service->delete();
                break;
            case self::UNMATCHED_PRODUCT_ERROR:
                $product->delete();
                break;
            case self::PRODUCT_MINIMUM_ERROR:
                $productData['quantity'] -= 1;
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $productData = array_merge($productData, [
                    'formula' => 'sublimate',
                    'numberOfColors' => 1,
                ]);
                break;
            default:
                // NOTE: There's some race condition that I can't find
        }

        // Get a response from the API
        $response = $this->post('transferPrice', [ 'userId' => $customerId ] + $productData);

        // Assert that the response matches what we expect
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
            case self::MISSING_SERVICE_ERROR:
            case self::UNMATCHED_PRODUCT_ERROR:
                $response->seeJson([ 'status' => 404 ]);
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $response->seeJson([ 'status' => 400 ]);
                break;
            case self::PRODUCT_MINIMUM_ERROR:
                $response->seeJson([
                    'status' => 400,
                    'detail' => "The quantity must be greater than or equal to $minimumQuantity - the product minimum value.",
                    'source' => 'quantity',
                ]);
                break;
            default:
                $response->seeJsonStructure([
                    'unitPrice', 'servicePrice', 'totalPrice', 'productId', 'productName',
                ]);
        }
    }
}
