<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\PrintroTestCase;

class GetPrintroOrderTest extends PrintroTestCase
{
    const ORDER_NOT_FOUND_ERROR = 1;

    public function testGetsPrintroOrder()
    {
        $this->runOrderRequestTest();
    }

    public function testGetPrintroOrderFailsWhenOrderDoesNotExistWithProvidedOrderId()
    {
        $this->runOrderRequestTest(self::ORDER_NOT_FOUND_ERROR);
    }

    protected function runOrderRequestTest($errorType = 0)
    {
        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();
        $order = factory('App\Order')->make([
            'ship_to_id' => $customer->shippingAddresses->first()->id,
            'bill_to_id' => $customer->billingAddresses->first()->id,
        ]);
        $customer->orders()->save($order);

        $orderId = $order->id;

        if ($errorType === self::ORDER_NOT_FOUND_ERROR) {
            $order->delete();
        }

        $response = $this->get('printroOrder', [ 'orderId' => $orderId ]);

        $addressStructure = [
            'id',
            'userId',
            'companyName',
            'shippingName',
            'firstName',
            'lastName',
            'street1',
            'street2',
            'street3',
            'city',
            'region',
            'zipcode',
            'country',
            'phoneNumber',
            'faxNumber',
            'emailAddress',
            'addressType',
            'addressCim',
            'addressMis',
            'isDefault',
            'isResidential',
            'timeCreated',
        ];

        if ($errorType === self::ORDER_NOT_FOUND_ERROR) {
            $response->seeJson([ 'status' => 404 ]);
            return;
        }

        // TODO: Test results rather than just structure
        $response->seeJsonStructure([
            'orderId',
            'poNumber',
            'orderStatus',
            'orderDate',
            $order->is_open ? 'estimatedDeliveryDate' : 'deliveryDate',
            'shipDate',
            'shippingMethod',
            'shippingAddress' => $addressStructure,
            'billingAddress' => $addressStructure,
            'packages' => [ '*' => [
                'shippingMethod',
                'trackingNumber',
                'trackingUrl',
            ] ],
            'orderProducts' => [ '*' => [
                'productId',
                'productSku',
                'productName',
                'quantity',
                'unitPrice',
                'totalPrice',
                'services' => [ '*' => [
                    'serviceId',
                    'serviceName',
                    'quantity',
                    'unitPrice',
                    'totalPrice',
                    'serviceData',
                ] ],
                'designs' => [ '*' => [
                    'designId',
                    'designName',
                    'printroId',
                    'placement',
                    'height',
                    'width',
                    'gutter',
                    'fileId',
                    'designSizeId',
                    'colors',
                    'fields',
                ] ],
                'data' => [
                    'preflightType',
                    'backgroundColor',
                    'partName',
                    'attributes' => [
                        'ink',
                        'formula',
                        'layout',
                        'production',
                        'sheet-height',
                        'sheet-width',
                    ],
                ],
            ] ],
            'shippingPrice',
            'subtotal',
            'tax',
            'total',
        ]);

        $response->seeJson([
            'orderId' => $orderId,
        ]);
    }
}
