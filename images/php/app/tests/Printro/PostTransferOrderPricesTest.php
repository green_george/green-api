<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\PrintroTestCase;
use App\Product;

class PostTransferOrderPricesTest extends PrintroTestCase
{
    const MISSING_USER_ERROR = 1;
    const MISSING_ADDRESS_ERROR = 2;
    const MISSING_SERVICE_ERROR = 3;
    const UNMATCHED_PRODUCT_ERROR = 4;
    const PRODUCT_MINIMUM_ERROR = 5;
    const NONZERO_NUM_OF_COLORS = 6;

    public function testCalculatesTransferOrderPrices()
    {
        $this->runTransferOrderPriceCalculationTest();
    }

    public function testCalculateTransferOrderPricesFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runTransferOrderPriceCalculationTest(self::MISSING_USER_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenAddressDoesNotExistWithProvidedShippingAddressId()
    {
        $this->runTransferOrderPriceCalculationTest(self::MISSING_ADDRESS_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenServiceDoesNotExistWithProvidedServiceId()
    {
        $this->runTransferOrderPriceCalculationTest(self::MISSING_SERVICE_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenNoMatchingProductIsFound()
    {
        $this->runTransferOrderPriceCalculationTest(self::UNMATCHED_PRODUCT_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenBelowProductMinimumQuantity()
    {
        $this->runTransferOrderPriceCalculationTest(self::PRODUCT_MINIMUM_ERROR);
    }

    public function testCalculateTransferOrderPricesFailsWhenSublimateFormulaIsUsedWithNonZeroNumberOfColor()
    {
        $this->runTransferOrderPriceCalculationTest(self::NONZERO_NUM_OF_COLORS);
    }

    protected function runTransferOrderPriceCalculationTest($errorType = 0)
    {
        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();
        $address = $customer->shippingAddresses->first();
        $product = factory('App\Product')
            ->states('forPrintro')
            ->create();
        $service = $product->services->first();

        // Store info from models in case they get deleted in the next step
        $customerId = $customer->id;
        $addressId = $address->id;
        $serviceId = $service->id;
        $minimumQuantity = $product->meta->_minimum_quantity;
        $productData = [
            'formula' => $product->meta->{'@attribute:formula'},
            'width' => $product->width,
            'height' => $product->height,
            'numberOfColors' => $product->number_of_colors,
            'quantity' => $product->meta->_minimum_quantity,
            'services' => [
                [
                    'serviceId' => $serviceId,
                    'quantity' => 1,
                ],
            ],
        ];

        // Set up the situation
        Product::query()->matches(array_only($productData, [
            'formula', 'numberOfColors', 'width', 'height'
        ]))->where('id', '<>', $product->id)->delete(); // Delete all other matching products
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
                $customer->delete();
                break;
            case self::MISSING_ADDRESS_ERROR:
                $address->delete();
                break;
            case self::MISSING_SERVICE_ERROR:
                $service->delete();
                break;
            case self::UNMATCHED_PRODUCT_ERROR:
                $product->delete();
                break;
            case self::PRODUCT_MINIMUM_ERROR:
                $productData['quantity'] -= 1;
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $productData = array_merge($productData, [
                    'formula' => 'sublimate',
                    'numberOfColors' => 1,
                ]);
                break;
            default:
                // NOTE: There's some race condition that I can't find
        }

        // Get a response from the API
        $response = $this->post('transferOrderPrices', [
            'userId' => $customerId,
        	'shippingAddressId' => $addressId,
        	'shippingMethod' => 'FedEx Ground',
        	'orderProducts' => [ $productData ],
        ]);

        // Evaluate the response
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
            case self::MISSING_ADDRESS_ERROR:
            case self::MISSING_SERVICE_ERROR:
            case self::UNMATCHED_PRODUCT_ERROR:
                $response->seeJson([ 'status' => 404 ]);
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $response->seeJson([ 'status' => 400 ]);
                break;
            case self::PRODUCT_MINIMUM_ERROR:
                $response->seeJson([
                    'status' => 400,
                    'detail' => "The orderProducts.0.quantity must be greater than or equal to $minimumQuantity - the product minimum value.",
                    'source' => 'orderProducts.0.quantity',
                ]);
                break;
            default:
                $response->seeJsonStructure([
                    'subtotal', 'shippingPrice', 'tax', 'total'
                ]);
        }
    }
}
