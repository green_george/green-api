<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\PrintroTestCase;
use App\Order;

class PostTransferOrderTest extends PrintroTestCase
{
    public function testCreatesTransferOrder()
    {
        // Set up the database with a new Customer and Product (and any required relations like associated Services, PricingTables, etc.)
        $customer = factory('App\Customer')->create();
        $product = factory('App\Product')->states('forPrintro')->create();
        $service = $product->services->first();

        // Send off a POST request to create a new Order and get the response
        $response = $this->post('transferOrder', [
            'userId' => $customer->id,
            'billingAddressId' => $customer->billingAddresses->first()->id,
            'shippingAddressId' => $customer->shippingAddresses->first()->id,
            'shippingMethod' => 'FedEx Ground',
            'poNumber' => 'Some PO Number Here',
            'comments' => 'Some Comment Here',
            'orderProducts' => [
                [
                    'formula' => $product->meta->{'@attribute:formula'},
                    'width' => $product->width,
                    'height' => $product->height,
                    'numberOfColors' => $product->number_of_colors,
                    'quantity' => $product->meta->_minimum_quantity,
                    'data' => [],
                    'services' => [
                        [
                            'serviceId' => $service->id,
                            'quantity' => 1,
                            'serviceData' => [],
                        ]
                    ],
                    'designs' => [
                        [
                            'printroId' => "value",
                            'designUrl' => "value",
                            'designName' => "value",
                            'placement' => "value",
                            'width' => $product->width,
                            'height' => $product->height,
                            'colors' => [ 'key' => 'value' ],
                            'fields' => [ 'key' => 'value' ],
                            'data' => [ 'key' => 'value' ],
                        ]
                    ],
                ]
            ],
        ]);

        // Validate the response data (should just be the new order's id)
        $response->seeJsonStructure([ 'orderId' ]);

        // Check that the Order exists in the database
        $orderId = $response->response->getData()->orderId;
        $this->assertTrue(Order::where('id', $orderId)->exists());
    }

    // TODO: Edge cases
}
