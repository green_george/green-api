<?php

namespace Tests\Printro\TestCases;

use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

abstract class PrintroTestCase extends TestCase
{
    use DatabaseTransactions; // Automatically rolls back any database changes after each test

    const PREFIX = '/printro/v1';

    /**
     * Overridden "get" function to include auth and the printro url prefix
     *
     * @return $this
     */
    public function get($url, $data = [])
    {
        return $this->printroRequest('GET', $url, $data);
    }

    /**
     * Overridden "post" function to include auth and the printro url prefix
     *
     * @return $this
     */
    public function post($url, array $data = [], array $headers = [])
    {
        return $this->printroRequest('POST', $url, $data);
    }

    private function printroRequest($method, $url, $data = [])
    {
        return $this->auth()->json($method, sprintf("%s/$url", self::PREFIX), $data);
    }
}
