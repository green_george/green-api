<?php

namespace Tests\Printro\TestCases;

class GetOrdersTestCase extends PrintroTestCase
{
    const USER_NOT_FOUND_ERROR = 1;

    protected function runOrdersListTest($open = true, $errorType = 0)
    {
        $openOrClosed = $open ? 'open' : 'closed';

        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();

        $customerId = $customer->id;

        // Set up the situation
        switch ($errorType) {
            case self::USER_NOT_FOUND_ERROR:
                $customer->delete();
                break;
        }

        // Get a response from the API
        $response = $this->get("{$openOrClosed}OrderList", [
            'userId' => $customerId,
        ]);

        // Evaluate the response
        switch ($errorType) {
            case self::USER_NOT_FOUND_ERROR:
                $response->seeJson([ 'status' => 404 ]);
                break;
            default:
                $response->seeJsonStructure([
                    'currentPage',
                    'data' => [ '*' => [
                        'orderId',
                        'poNumber',
                        'orderDate',
                        'status',
                        'tax',
                        'subtotalPrice',
                        'totalPrice',
                        'packages',
                    ]],
                    'firstPageUrl',
                    'from',
                    'lastPage',
                    'lastPageUrl',
                    'nextPageUrl',
                    'path',
                    'perPage',
                    'prevPageUrl',
                    'to',
                    'total',
                ]);
        }
    }
}
