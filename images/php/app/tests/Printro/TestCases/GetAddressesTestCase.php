<?php

namespace Tests\Printro\TestCases;

class GetAddressesTestCase extends PrintroTestCase
{
    const MISSING_USER_ID_ERROR = 1;
    const INVALID_USER_ID_ERROR = 2;
    const USER_NOT_FOUND_ERROR = 3;

    protected function runAddressesTest($isBilling = false, $errorType = 0)
    {
        $addressType = $isBilling ? 'billingAddresses' : 'shippingAddresses';

        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();

        // Define the default request data
        $requestData = [
            'userId' => $customer->id,
        ];

        // Init request/database to fire errors
        switch($errorType) {
            case self::MISSING_USER_ID_ERROR:
                unset($requestData['userId']);
                break;
            case self::INVALID_USER_ID_ERROR:
                $requestData['userId'] = -1;
                break;
            case self::USER_NOT_FOUND_ERROR:
                $customer->delete();
                break;
        }

        // Send and receive to/from API address endpoint
        $response = $this->get($addressType, $requestData);

        // Make sure we got the expected response
        $addresses = $customer->$addressType;
        switch($errorType) {
            case self::MISSING_USER_ID_ERROR:
            case self::INVALID_USER_ID_ERROR:
                $response->seeJson([ 'status' => 400 ]);
                break;
            case self::USER_NOT_FOUND_ERROR:
                $response->seeJson([ 'status' => 404 ]);
                break;
            default:
                $response->seeJson($addresses->map(function($address) use ($isBilling) {
                    $billingDetails = (
                        $isBilling ? $address->getAuthorizeNetCreditCardDetails() + [
                            'paymentMethod' => $address->payment_method_type,
                            'paymentMethodDescription' => $address->payment_method_description,
                        ] : []
                    );

                    return array_merge([
                        'id' => $address->id,
                        'userId' => $address->user_id,
                        'companyName' => $address->company_name,
                        'shippingName' => $address->shipping_name,
                        'firstName' => $address->first_name,
                        'lastName' => $address->last_name,
                        'street1' => $address->street1,
                        'street2' => $address->street2,
                        'street3' => $address->street3,
                        'city' => $address->city,
                        'region' => $address->region,
                        'zipcode' => $address->zipcode,
                        'country' => $address->country,
                        'phoneNumber' => $address->phone_number,
                        'faxNumber' => $address->fax_number,
                        'emailAddress' => $address->email_address,
                        'addressType' => $address->type,
                        'addressCim' => $address->cim,
                        'addressMis' => $address->mis,
                        'isDefault' => $address->is_default,
                        'isResidential' => $address->is_residential,
                        'timeCreated' => $address->time_created,
                    ], $billingDetails);
                })->toArray());
        }
    }
}
