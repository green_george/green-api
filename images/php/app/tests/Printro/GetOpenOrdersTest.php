<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\GetOrdersTestCase;

class GetOpenOrdersTest extends GetOrdersTestCase
{
    public function testGetsOpenOrders()
    {
        $this->runOrdersListTest(true);
    }

    public function testGetOpenOrdersFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runOrdersListTest(true, self::USER_NOT_FOUND_ERROR);
    }
}
