<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\GetOrdersTestCase;

class GetClosedOrdersTest extends GetOrdersTestCase
{
    public function testGetsClosedOrders()
    {
        $this->runOrdersListTest(false);
    }

    public function testGetClosedOrdersFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runOrdersListTest(false, self::USER_NOT_FOUND_ERROR);
    }
}
