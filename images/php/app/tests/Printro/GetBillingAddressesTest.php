<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\GetAddressesTestCase;

class GetBillingAddressesTest extends GetAddressesTestCase
{
    public function testGetsBillingAddresses()
    {
        $this->runAddressesTest(true);
    }

    public function testGetBillingAddressesFailsWhenNoUserIdIsProvided()
    {
        $this->runAddressesTest(true, self::MISSING_USER_ID_ERROR);
    }

    public function testGetBillingAddressesFailsWhenUserIdIsInvalid()
    {
        $this->runAddressesTest(true, self::INVALID_USER_ID_ERROR);
    }

    public function testGetBillingAddressesFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runAddressesTest(true, self::USER_NOT_FOUND_ERROR);
    }
}
