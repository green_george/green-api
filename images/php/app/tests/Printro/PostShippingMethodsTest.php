<?php

namespace Tests\Printro;

use Tests\Printro\TestCases\PrintroTestCase;
use App\Product;

class PostShippingMethodsTest extends PrintroTestCase
{
    const MISSING_USER_ERROR = 1;
    const MISSING_ADDRESS_ERROR = 2;
    const UNMATCHED_PRODUCT_ERROR = 3;
    const NONZERO_NUM_OF_COLORS = 4;

    public function testGetsShippingMethods()
    {
        $this->runShippingMethodsTest();
    }

    public function testGetShippingMethodsFailsWhenUserDoesNotExistWithProvidedUserId()
    {
        $this->runShippingMethodsTest(self::MISSING_USER_ERROR);
    }

    public function testGetShippingMethodsFailsWhenAddressDoesNotExistWithProvidedAddressId()
    {
        $this->runShippingMethodsTest(self::MISSING_ADDRESS_ERROR);
    }

    public function testGetShippingMethodsFailsWhenNoMatchingProductIsFound()
    {
        $this->runShippingMethodsTest(self::UNMATCHED_PRODUCT_ERROR);
    }

    public function testGetShippingMethodsFailsWhenSublimateFormulaIsUsedWithNonZeroNumberOfColors()
    {
        $this->runShippingMethodsTest(self::NONZERO_NUM_OF_COLORS);
    }

    protected function runShippingMethodsTest($errorType = 0)
    {
        // Insert, then get models from the database
        $customer = factory('App\Customer')->create();
        $address = $customer->shippingAddresses->first();
        $product = factory('App\Product')
            ->states('forPrintro')
            ->create();

        // Save specific values for the request (in case models are deleted)
        $customerId = $customer->id;
        $addressId = $address->id;
        $minimumQuantity = $product->meta->_minimum_quantity;

        // Default product request data (changed to cause errors)
        $productData = [
            'formula' => $product->meta->{'@attribute:formula'},
            'width' => $product->width,
            'height' => $product->height,
            'numberOfColors' => $product->number_of_colors,
            'quantity' => $product->meta->_minimum_quantity,
        ];

        // Set up the situation
        Product::query()->matches(array_only($productData, [
            'formula', 'numberOfColors', 'width', 'height'
        ]))->where('id', '<>', $product->id)->delete(); // Delete all other matching products
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
                $customer->delete();
                break;
            case self::MISSING_ADDRESS_ERROR:
                $address->delete();
                break;
            case self::UNMATCHED_PRODUCT_ERROR:
                $product->delete();
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $productData = array_merge($productData, [
                    'formula' => 'sublimate',
                    'numberOfColors' => 1,
                ]);
                break;
        }

        // Get a response from the API
        $response = $this->post('shippingMethods', [
            'userId' => $customerId,
            'addressId' => $addressId,
            'products' => [ $productData ],
        ]);

        // Evaluate the response
        switch ($errorType) {
            case self::MISSING_USER_ERROR:
            case self::MISSING_ADDRESS_ERROR:
            case self::UNMATCHED_PRODUCT_ERROR:
                $response->seeJson([ 'status' => 404 ]);
                break;
            case self::NONZERO_NUM_OF_COLORS:
                $response->seeJson([ 'status' => 400 ]);
                break;
            default:
                $response->seeJsonStructure(['*' => [
                    'method',
                    'price',
                    'estimatedDelivery'
                ]]);
        }
    }
}
