<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as LumenTestCase;

abstract class TestCase extends LumenTestCase
{
    /**
     * Reusable logic to test beyond the auth middleware.
     */
    protected function auth() {
        $user = factory('App\User')->create();
        return $this->actingAs($user);
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
