# README

## Server Setup Logic Flow
1. `package.json`'s start script
2. `bin/app`
3. `web/index.js` configures API routes using callbacks found in `routes.js`

## Example Route Logic Flow
1. Request is picked up by a matching route. For example:
	1. `POST localhost:8000/cart` (+ JSON data)
	2. Matching callback in `web/routes.js` gets run, providing request data
	3. Each callback has been injected with any dependencies it may need, including parts from `domain`

## TODO
- Reverse dependency direction between web and domain?
- Also, domain should be the starting point and web should plug into it somehow?
