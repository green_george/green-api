/*
MIT License

Copyright (c) 2016 Matt Paine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

(Modified to gracefully handle undefined values)
*/

// (Modified to ignore/gracefully handle undefined values)
const serialize = (v, options) => {
  if (!options) {
      options = {
          metakey: '__phpSerializedObject__',
      };
  }
  var ret = '';
  if (v === null) {
      ret = 'N;';
  } else if (typeof v === 'boolean') {
      ret = 'b:' + (v ? '1' : '0') + ';';
  } else if (typeof v === 'number' || (typeof v === 'string' && "" + (+v) === v)) {
      if (v % 1 === 0) {
          ret = 'i:' + v + ';';
      } else if (isNaN(v)) {
          ret = 'd:NAN;';
      } else {
          ret = 'd:' + v.toFixed(16) + ';';
      }
  } else if (typeof v === "string") {
      ret = "s:" + Buffer.byteLength(v, 'utf8') + ':"' + v + '";';
  } else if (v !== undefined && v[options.metakey]) {
      var meta = v[options.metakey];
      var properties = Object.keys(v).filter(k => { return k !== options.metakey; }).map(k => {
          var key = k;
          if (meta.access && meta.access[k] && meta.access[k] === "protected") {
              key = "\0*\0" + k;
          }
          else if (meta.access && meta.access[k] && meta.access[k] === "private") {
              key = "\0" + meta.name + "\0" + k;
          }
          var serializedValue = serialize(v[k]);
          return serializedValue !== '' ? serialize(key) + serializedValue : '';
      }).filter(s => s !== '');
      ret = 'O:' + meta.name.length + ':"' + meta.name + '":' + properties.length + ':{' + properties.join('') + '}';
  } else if (typeof v === 'object') {
      var properties = Object.keys(v).map(k => {
          return serialize(k, options) + serialize(v[k], options);
      });
      ret = 'a:' + properties.length + ':{' + properties.join('') + '}';
  }
  return ret;
}

export default serialize
