export default ({
  Sequelize,
  Model,
  sequelize,
  serializedPHPField,
}) => {
  class CartProduct extends Model {}
  CartProduct.init({
    id: {
      type: Sequelize.INTEGER(32),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    storeId: { type: Sequelize.INTEGER(32), defaultValue: 9 },
    productId: Sequelize.INTEGER(32),

    productDesigns: serializedPHPField('productDesigns', []),
    productServices: serializedPHPField('productServices', []),
    productData: serializedPHPField('productData', {}),
    productUploads: serializedPHPField('productUploads', []),

    productQuantity: Sequelize.INTEGER(16),
    liveImage: Sequelize.TEXT('long'), // NOTE: Can set to image url
  }, {
    sequelize,
    modelName: 'wm_cart_products',
    underscored: true,
    timestamps: false,
  })
  return CartProduct
}
