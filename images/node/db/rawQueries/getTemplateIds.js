export default (sequelize) => async ({ templateId }) => {
  const result = Object.values(
    await sequelize.query(`
      SELECT design_id AS id, gd_id, design_id, -1 AS file_id, placement
      FROM wm_printro_template_link
      WHERE type = 1 AND printro_id = ?
      LIMIT 1
    `, {
      raw: true,
      type: sequelize.QueryTypes.SELECT,
      replacements: [ templateId ],
    })
  )[0]

  return result
}
