export default (sequelize) => async ({ printroId }) => {
  const result = Object.values(
    await sequelize.query(`
      SELECT c.gd_id
      FROM wm_colors c
      WHERE id = (
        SELECT color_id
        FROM wm_color_id_map
        WHERE printro_id = ?
        LIMIT 1
      )
    `, {
      raw: true,
      type: sequelize.QueryTypes.SELECT,
      replacements: [ printroId ],
    })
  )[0]

  return result && result.gd_id
}
