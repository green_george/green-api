const matchingProductsQuery = `
SET @width = ?;
SET @height = ?;
SET @formula = ?;
SET @numOfColors = ?;
SET @quantity = ?;

-- Map "@numOfColors" to a list of possible "ink" values in the database
DROP TEMPORARY TABLE IF EXISTS ink;
CREATE TEMPORARY TABLE ink (
    num_of_colors INT NOT NULL,
    meta_value VARCHAR(16) NOT NULL PRIMARY KEY
);
INSERT INTO ink (num_of_colors, meta_value) VALUES
    (0, 'foil'),
    (0, 'sublimate'),
    (0, 'freedom'),
    (0, 'PR'),
    (@numOfColors, CONCAT(@numOfColors, 'C')),
    (@numOfColors, CONCAT(@numOfColors, 'color'))
;

-- Gather the possible product matches using @width, @height, @formula and the "ink" temp table above
DROP TEMPORARY TABLE IF EXISTS matching_products;
CREATE TEMPORARY TABLE matching_products (
    product_id INT NOT NULL PRIMARY KEY
);
INSERT INTO matching_products (product_id) (
    SELECT wm_products.id AS product_id
    FROM wm_products
    WHERE
        product_model_id = 4 -- type of product = sales
        AND LEAST(product_width, product_height) >= LEAST(@width, @height)
        AND GREATEST(product_width, product_height) >= GREATEST(@width, @height)
        AND EXISTS (
            SELECT 0
            FROM wm_product_meta
            WHERE product_id = wm_products.id
                AND meta_key = '@attribute:formula'
                AND meta_value = @formula
        )
        AND EXISTS (
            SELECT 0
            FROM wm_product_meta
            WHERE product_id = wm_products.id
                AND meta_key = '@attribute:ink'
                AND meta_value IN (
                    SELECT meta_value
                    FROM ink
                    WHERE num_of_colors = @numOfColors
                )
        )
);

-- Gather the most recent pricing tables for each matching product above
DROP TEMPORARY TABLE IF EXISTS most_recent_pricing_tables;
CREATE TEMPORARY TABLE most_recent_pricing_tables (
    table_id INT NOT NULL PRIMARY KEY
);
INSERT INTO most_recent_pricing_tables (table_id) (
    SELECT MAX(ppt.id)
    FROM wm_product_pricing_tables ppt
    WHERE ppt.product_id IN (SELECT product_id FROM matching_products)
    GROUP BY product_id
);

-- Gather additional info on each matching product (total costs
SELECT
    p.id,
    p.product_name,
    pp.product_price,
    COALESCE(sp.service_price, 0) AS service_price,
    pp.product_price + COALESCE(sp.service_price, 0) AS total,
    (
        SELECT meta_value
        FROM wm_product_meta pm
        WHERE
            pm.product_id = pp.product_id
            AND meta_key = '_minimum_quantity'
        LIMIT 1
    ) AS min_quantity
FROM
    wm_products p
    INNER JOIN (
        -- Product's price at the given quantity
        SELECT ppt.product_id, ptd.price * @quantity AS product_price
        FROM
            wm_product_pricing_tables ppt
            INNER JOIN wm_pricing_table_data ptd ON ptd.pricing_table_id = ppt.pricing_table_id
        WHERE
            -- Limit to only the most recently-created pricing table
            ppt.id IN (SELECT table_id FROM most_recent_pricing_tables)
            AND @quantity >= ptd.first_quantity
            AND (ptd.last_quantity = -1 OR @quantity <= ptd.last_quantity)
    ) pp ON pp.product_id = p.id
    LEFT JOIN (
        -- Price of required services for that product at the given quantity
        SELECT
            ps.product_id,
            SUM(ptd.price) AS service_price
        FROM wm_product_services ps
            INNER JOIN wm_service_pricing_tables spt ON spt.service_id = ps.service_id
            INNER JOIN wm_pricing_table_data ptd ON ptd.pricing_table_id = spt.pricing_table_id
        WHERE ps.product_id IN (SELECT product_id FROM matching_products)
            AND @quantity >= ptd.first_quantity
            AND (ptd.last_quantity = -1 OR @quantity <= ptd.last_quantity)
        GROUP BY ps.product_id
    ) sp ON sp.product_id = pp.product_id
ORDER BY total;

DROP TEMPORARY TABLE matching_products;
DROP TEMPORARY TABLE most_recent_pricing_tables;
DROP TEMPORARY TABLE ink;
`

export default (sequelize) => async ({
  width, height, formula, numberOfColors = 0, quantity = 1
}) => Object.values(
  (await sequelize.query(matchingProductsQuery, {
    raw: true,
    type: sequelize.QueryTypes.SELECT,
    replacements: [ width, height, formula, numberOfColors, quantity ],
  }))[14]).map(({
    id,
    product_name,
    product_price,
    service_price,
    total,
    min_quantity,
  }) => ({
    id,
    productName: product_name,
    productPrice: +product_price,
    servicePrice: +service_price,
    total: +total,
    minQuantity: +min_quantity,
  })
)
