export default (sequelize) => async ({ clipartId }) => {
  const result = Object.values(
    await sequelize.query(`
      SELECT gd_id
      FROM wm_printro_template_link
      WHERE type = 2 AND printro_id = ?
      LIMIT 1
    `, {
      raw: true,
      type: sequelize.QueryTypes.SELECT,
      replacements: [ clipartId ],
    })
  )[0]

  return result && result.gd_id
}
