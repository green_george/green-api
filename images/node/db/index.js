import { Sequelize, Model } from 'sequelize'
import { promisify } from 'util'
import phpSerialize from './utils/phpSerialize'
import phpUnserialize from './utils/phpUnserialize'
import getMatchingProductPrices from './rawQueries/getMatchingProductPrices'
import getMatchingColor from './rawQueries/getMatchingColor'
import getTemplateIds from './rawQueries/getTemplateIds'
import getClipartId from './rawQueries/getClipartId'

export default async ({
  DB_DATABASE, MYSQL_USER, MYSQL_PASSWORD, DB_HOST
}) => {
  const models = [
    ['Cart', (await import('./Cart')).default],
    ['CartProduct', (await import('./CartProduct')).default],
  ]

  const sequelize = new Sequelize(DB_DATABASE, MYSQL_USER, MYSQL_PASSWORD, {
    host: DB_HOST,
    dialect: 'mysql',
    dialectOptions: {
      multipleStatements: true
    }
  })

  const dashboardSequelize = new Sequelize('wm_dashboard', MYSQL_USER, MYSQL_PASSWORD, {
    host: DB_HOST,
    dialect: 'mysql',
  })

  const fmFlattenObjects = (obj, keyPath = '', outObj = {}) => {
    Object.entries(obj).forEach(([key, value]) => {
      if (typeof value === 'object') {
        Object.assign(outObj, fmFlattenObjects(value, `${keyPath}@${key}:`, outObj))
        return
      }
      outObj[keyPath + key] = value
    })
    return outObj
  }
  const camelToSnakeCase = str => str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`)
  const snakeCaseKeys = o => Object.fromEntries(Object.entries(o).map(
    ([k, v]) => [camelToSnakeCase(k), v]
  ))
  const toSerializable = o => Object.assign({}, fmFlattenObjects(snakeCaseKeys(o)), { __phpSerializedObject__: { name: 'stdClass' } })

  const serializedPHPField = (key, defaultVal) => ({
    type: Sequelize.TEXT('long'),
    allowNull: false,
    set(val = defaultVal) {
      try {
        if (!Array.isArray(val)) {
          const data = toSerializable(val)
          this.setDataValue(key, phpSerialize(data))
        } else {
          const data = val.map(toSerializable)
          this.setDataValue(key, phpSerialize(data))
        }
      } catch {
        this.setDataValue(key, phpSerialize([]))
      }
    },
    get() {
      try {
        return Object.values(phpUnserialize(this.getDataValue(key)))
      } catch {
        return []
      }
    },
  })

  const injections = {
    Sequelize,
    Model,
    sequelize,
    serializedPHPField,
  }

  const exportObj = Object.fromEntries(
    models.map(([key, fn]) => [key, fn(injections)])
  )
  exportObj.getMatchingProductPrices = getMatchingProductPrices(sequelize)
  exportObj.getMatchingColor = getMatchingColor(dashboardSequelize)
  exportObj.getTemplateIds = getTemplateIds(dashboardSequelize)
  exportObj.getClipartId = getClipartId(dashboardSequelize)

  // Configure relationships
  exportObj.CartProductsAssociation = exportObj.Cart.hasMany(exportObj.CartProduct, { as: 'cartProducts', foreignKey: 'cartId', sourceKey: 'id' })
  exportObj.CartProduct.belongsTo(exportObj.Cart, { as: 'cart', foreignKey: 'cartId', targetKey: 'id' })

  // Attempt to connect to database
  const MAX_ERROR_COUNT = 3
  const sleep = promisify(setTimeout)
  let errorCount = 0
  while (errorCount < MAX_ERROR_COUNT) {
    try {
      // await sequelize.sync()
      break
    } catch (e) {
      errorCount += 1
      console.error(e)
      await sleep(3000)
    }
    if (errorCount >= MAX_ERROR_COUNT) {
      await sleep(7000)
      throw Error(`Failed to connect to database after ${MAX_ERROR_COUNT} tries`)
    }
  }

  return exportObj
}
