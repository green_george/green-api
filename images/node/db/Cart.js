export default ({
  Sequelize,
  Model,
  sequelize,
}) => {
  class Cart extends Model {}
  Cart.init({
    id: {
      type: Sequelize.INTEGER(32),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    storeId: { type: Sequelize.INTEGER, defaultValue: 9 },
    userId: Sequelize.INTEGER,
    cartSessionSerial: { type: Sequelize.STRING(32), defaultValue: '' },
    cartTypeId: { type: Sequelize.TINYINT(8), defaultValue: 1 },
    timeCreated: Sequelize.INTEGER(32),
    timeUpdated: Sequelize.INTEGER(32),
  }, {
    sequelize,
    modelName: 'wm_carts',
    underscored: true,
    timestamps: false,
  })
  return Cart
}
