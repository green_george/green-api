import fetch from 'node-fetch'
import buildDb from '../db'
import auth from './middleware/auth'
import validation from './middleware/validation'
import {
  get as getCart,
  create as createCart,
} from './cart'

// Set up middleware and route handlers
export default async (app, opts) => {
  const db = await buildDb(opts)
  const authMiddleware = await auth()

  // Logic will hit this middleware first to make sure we have a valid token
  app.use(authMiddleware)

  // Routes

  // TODO: Test running this again
  app.get('/', (req, res) => res.json({ hello: 'world!' }))
  app.get('/carts', getCart({ fetch, ...db }))
  app.post('/carts', createCart({ fetch, ...db }))

  app.use(validation)
}
