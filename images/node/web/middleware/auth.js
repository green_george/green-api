import jwt from 'jsonwebtoken'
import { Sequelize, Model } from 'sequelize'
import { promisify } from 'util'

const { DB_HOST, DB_AUTH_DATABASE, MYSQL_USER, MYSQL_PASSWORD } = process.env

const sequelize = new Sequelize(DB_AUTH_DATABASE, MYSQL_USER, MYSQL_PASSWORD, {
  host: DB_HOST,
  dialect: 'mysql',
})

export default async () => {
  class OauthAccessToken extends Model {}
  OauthAccessToken.init({
    user_id: Sequelize.INTEGER,
  }, {
    sequelize,
    modelName: 'oauth_access_tokens',
    timestamps: false,
  })

  // Attempt to connect to database
  const MAX_ERROR_COUNT = 3
  const sleep = promisify(setTimeout)
  let errorCount = 0
  while (errorCount < MAX_ERROR_COUNT) {
    try {
      await sequelize.sync()
      break
    } catch {
      errorCount += 1
      await sleep(3000)
    }
    if (errorCount >= MAX_ERROR_COUNT) {
      await sleep(7000)
      throw Error(`Failed to connect to database after ${MAX_ERROR_COUNT} tries`)
    }
  }

  // Actual middleware function
  return async (request, response, next) => {
    const { headers } = request
    if (headers.authorization) {
      const authorization = headers.authorization
      const comp = authorization.split(' ')
      if (comp.length == 2 && comp[0] == 'Bearer') {
        const token = comp[1]
        const { jti } = jwt.decode(token)

        const access_token = await OauthAccessToken.findByPk(jti)
        if (access_token) {
          request.user_id = access_token.user_id
          next()
          return
        }
      }
    }

    next(new Error('Endpoint requires a valid token'))
  }
}
