import { validationResult } from 'express-validator'

export { check } from 'express-validator'

export class ValidationError extends Error {
  static name = 'ValidationError'

  constructor(messages) {
    super(messages.join('\n'))
    this.messages = messages
  }

  get message() {
    return this.messages.join('\n')
  }
}

export default (req, res, next) => {
  // Finds the validation errors in this request and wraps them in an object with handy functions
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    next(new ValidationError(errors.array()))
  }
  next()
}
