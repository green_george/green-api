import injectUploadDesignAsync from './uploadDesignAsync'

// Dependency Injection
const injectMapColorsAsync = ({ mapAsync, getMatchingColor }) =>
  // mapColorsAsync closure (map props to c1, c2, etc)
  async (colors) => colors && Object.fromEntries((await mapAsync(Object.entries(colors),
    ([c, printroId]) => getMatchingColor({ printroId })
  )).map((v, idx) => [`c${idx + 1}`, v]))

const injectMapFieldsAsync = ({ mapAsync, getClipartId }) =>
  async (fields) => fields && Object.fromEntries(await mapAsync(Object.entries(fields),
    async ([k, v]) => [k, k === 'clipart' ? await getClipartId({ clipartId: v }) : v]
  ))

// Dependency Injection
export default (di) => {
  const { mapAsync, fixDesignUrl, getTemplateIds } = di
  const mapColorsAsync = injectMapColorsAsync(di)
  const mapFieldsAsync = injectMapFieldsAsync(di)
  const uploadDesignAsync = injectUploadDesignAsync(di)

  // mapProductDesignsAsync closure
  return (designs = []) => mapAsync(designs, async ({
    templateId, placement, colors, fields, data, designUrl, ...rest
  }) => {
    const shared = {
      color: await mapColorsAsync(colors),
      field: await mapFieldsAsync(fields),
      data,
      ...rest
    }

    const design = templateId
      ? await getTemplateIds({ templateId })
      : Object.assign(await uploadDesignAsync(shared), {
        designUrl: fixDesignUrl(designUrl),
        placement,
      })

    // create flat list of props like @color:c1, @color:c2, etc...
    return Object.assign({}, shared, design)
  })
}
