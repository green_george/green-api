import convert from 'xml-js'

const getURLExtension = (url) =>
  url.split(/\#|\?/)[0].split('.').pop().trim()

const fakeResponse = {
  AuthorizationResponse: {
    RequestResponse: {
      Response: {
        DesignID: { _text: '0' },
        DesignSizeID: { _text: '0' },
      },
    },
  },
}

const capitalizeFirstLetter = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1)

  // Dependency Injection
export default ({ fixDesignUrl, fetch }) => {
  const fetchXMLAsJson = (...args) => fetch(...args)
    .then(response => response.text())
    .then(str => JSON.parse(convert.xml2json(str, { compact: true, spaces: 4 })))

  // uploadDesignAsync closure
  return async ({
    designName,
    width,
    height,
    designUrl,
    printroId,
    color,
    field,
    data,
  }) => {
    const renderAttributes = Object.entries({ color, field, data }).flatMap(([key, obj]) => {
      if (obj === undefined || obj === null) return []

      const type = capitalizeFirstLetter(key)
      return Object.entries(obj).map(([key, value]) => (
        { type, key, value }
      ))
    })

    // If in "dev", skip API call and just get a random existing design
    const designResponse = process.env.NODE_ENV !== 'production'
      ? fakeResponse
      : await fetchXMLAsJson(`http://api.greendistro.com/api/xml/xml/`, {
        method: 'POST',
        body: `
          <?xml version="1.0"?>
          <!-- WRAPPER REQUEST TO AUTHORIZE API -->
          <AuthorizationRequest Key="ec590bcdeeec3761b467140c62df8ce5">
              <AddDigitalAssetRequest>
                  <StoreID>9</StoreID>
                  <DigitalAsset>
                      <NotPrintable>TRUE</NotPrintable>
                      <Name>${designName}</Name>
                      <Width>${width}</Width>
                      <Height>${height}</Height>
                      <FileURL>${fixDesignUrl(designUrl)}</FileURL>
                      <FileExtension>${getURLExtension(designUrl)}</FileExtension>
                      <PrintroID>${printroId}</PrintroID>
                      ${renderAttributes.map(({ type, key, value }) => `
                      <RenderAttribute>
                        <Type>${type}</Type>
                        <Key>${key}</Key>
                        <Value>${value}</Value>
                      </RenderAttribute>
                      `).join('\n')}
                  </DigitalAsset>
              </AddDigitalAssetRequest>
          </AuthorizationRequest>
        `,
      })

    const {
      DesignID: { _text: id },
      DesignSizeID: { _text: designSizeId },
    } = designResponse.AuthorizationResponse.RequestResponse.Response

    return {
      id: +id,
      designSizeId: +designSizeId
    }
  }
}
