import { mapAsync, fixDesignUrl } from '../../utils'
import injectMapCartProductsAsync from './mapCartProductsAsync'

// Dependency Injection
export const create = ({ Cart, CartProduct, CartProductsAssociation, ...passThrough }) => {
  const mapCartProductsAsync = injectMapCartProductsAsync({
    mapAsync,
    fixDesignUrl,
    ...passThrough,
  })

  // create closure
  return async (req, res, next) => {
    try {
      const { userId, products } = req.body

      // Build Cart model data
      const now = Math.floor(Date.now() / 1000)

      const cartProducts = await mapCartProductsAsync(products)

      const cartData = {
        storeId: 9,
        userId,
        cartSessionSerial: '',
        cartTypeId: 1,
        timeCreated: now,
        timeUpdated: now,
      }

      // Save Cart with associated CartProducts
      const [ model ] = await Cart.findOrCreate({
        where: { storeId: 9, userId, cartTypeId: 1 },
        defaults: cartData,
        include: [ CartProductsAssociation ],
      })

      const d = await mapAsync(cartProducts, async cp => {
        cp.cartId = model.id
        return CartProduct.create(cp)
      })

      res.status(201).json(Object.assign({ id: model.id }, cartData, { cartProducts }))
    } catch (e) {
      next(e)
    }
  }
}

/*
// Keeping these around for reference
export const createValidation = (check) => [
  // username must be an email
  check('username').isEmail(),
  // password must be at least 5 chars long
  check('password').isLength({ min: 5 })
]
*/

export const get = ({ Cart, CartProduct }) => async (req, res) => {
  res.json({
    carts: await Cart.findAll({ limit: 10, include: [
      { model: CartProduct, as: 'cartProducts' }
    ]})
  })
}
