import injectMapProductDesignsAsync from './mapProductDesignsAsync'

// Dependency Injection
export default (di) => {
  const {
    mapAsync,
    fixDesignUrl,
    getMatchingProductPrices,
  } = di

  const mapProductDesignsAsync = injectMapProductDesignsAsync(di)

  // mapCartProductsAsync closure
  return (products) => mapAsync(products, async ({
    designs,
    services,
    data,
    quantity,
    ...productSearch
  }) => {
    const matchingProducts = await getMatchingProductPrices({ quantity, ...productSearch })
    const matchingProduct = matchingProducts.find(p => p.minQuantity <= quantity)
    // TODO: Add validation for matching products and min quantity
    const firstDesign = designs[0]

    return {
      storeId: 9,
      productId: matchingProduct.id,
      productDesigns: await mapProductDesignsAsync(designs),
      productServices: services,
      productData: data,
      productQuantity: quantity,
      productUploads: [], // Leave empty, we are already handling uploads here
      liveImage: fixDesignUrl(firstDesign && firstDesign.designUrl),
    }
  })
}
