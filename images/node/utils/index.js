const escapeHtml = (text) => {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;',
  }
  return text.replace(/[&<>"']/g, m => map[m])
}

const blobStorageDomain = 'https://printroprod.blob.core.windows.net'
const escapeRegExp = s => s.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& = whole matched string
const blobStoragePattern = new RegExp(`^${escapeRegExp(blobStorageDomain)}`)

export const fixDesignUrl = s => escapeHtml(s) + (blobStoragePattern.test(s)
  ? escapeHtml('?sv=2018-03-28&ss=bf&srt=co&sp=rl&st=2019-01-21T19%3A42%3A41Z&se=2024-01-22T19%3A42%3A00Z&sig=dOizSEk%2Bq9E3T%2FTGpnmOa3LaaqOv%2FUp1o%2BbqZrAEOCA%3D')
  : ''
)

export const mapAsync = (arr, fn) => Promise.all(arr.map(fn))
