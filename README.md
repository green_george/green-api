# green-api

## Introduction

Green Applications' primary API.

This document outlines developer processes and architecture. For API-level documentation, see the [Postman Collection](https://green-applications.postman.co/collections/5527693-508ce7e3-a1fe-4070-ac7e-654383d03471?workspace=9fc30303-fe84-4343-8b8b-87f22e1aa433). Also, a [Trello board](https://trello.com/b/tV1829um/green-api) is available to use for the current development task list.

## Table of contents
- [Introduction](#markdown-header-introduction)
- [Table of Contents](#markdown-header-table-of-contents)
- [Setup](#markdown-header-setup)
    - [Environment Configuration](#markdown-header-environment-configuration)
    - [Docker for Mac](#markdown-header-docker-for-mac)
    - [Docker for Windows](#markdown-header-docker-for-windows)
    - [Docker for Linux](#markdown-header-docker-for-linux)
    - [Build and Run](#markdown-header-build-and-run)
        - [Development and Staging](#markdown-header-development-and-staging)
        - [Production](#markdown-header-production)
    - [Stop Everything](#markdown-header-stop-everything)
    - [Database Setup](#markdown-header-database-setup)
- [Running Commands](#markdown-header-running-commands)
    - [Single Command Examples](#markdown-header-single-command-examples)
    - [Multiple Command Example](#markdown-header-multiple-command-example)
- [Making Changes](#markdown-header-making-changes)
    - [GitFlow Workflow](#markdown-header-gitflow-workflow)
    - [Versioning](#markdown-header-versioning)
    - [Coming From Traditional PHP](#markdown-header-coming-from-traditional-php)
    - [Changing Dependencies](#markdown-header-changing-dependencies)
    - [Architecture](#markdown-header-architecture)
        - [GET Order Example](#markdown-header-get-order-example)
        - [Links](#markdown-header-links)
    - [Testing](#markdown-header-testing)
- [Additional Notes](#markdown-header-additional-notes)
    - [Docker](#markdown-header-docker-notes)
    - [Authentication](#markdown-header-authentication-notes)
    - [Routes](#markdown-header-route-notes)
    - [Database](#markdown-header-database-notes)

## Setup

### Environment Configuration
When running this project from a Linux host machine, file permissions can be problematic. After cloning the git repository, be sure to modify file permissions before continuing setup.

Ex.
- `git clone https://username@bitbucket.org/green_george/green-api.git` (replace "username" with your bitbucket username)
- `git checkout develop` (replace "develop" with the appropriate branch for the target environment)

For development, you should also run `git flow init` from a git terminal or initialize Gitflow from within your Git client since this project is intended to follow the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). (See also: [Making Changes](#markdown-header-making-changes))

The rest of the setup steps should be performed via docker...

### [Docker for Mac](https://docs.docker.com/docker-for-mac/)
### [Docker for Windows](https://docs.docker.com/docker-for-windows/)
### [Docker for Linux](https://docs.docker.com/engine/installation/linux/)
Ex. `apt install docker-compose`

### Build and Run

When the following command is run, nginx + php-fpm services are started to serve up the API (dev/staging configuration also starts up its own MySQL instance). NOTE: Composer may take a while to finish pulling dependencies into the `vendors` folder, but it runs automatically in the background on start.

#### Development and Staging
```bash
docker-compose up --build -d
```

#### Production
```bash
docker-compose -f docker-compose.production.yml up --build -d
```


### Stop Everything
```bash
docker-compose down
```

### Database Setup
NOTE: The sql file schema should eventually be merged into migrations and any row data into seeders.

The `api` and `wm_dashboard` databases need to exist and user privileges need to be set for each database (see the docker-compose*.yml file(s) for the database users)

- As the root user, you can run the following SQL to set up local and staging:
    - `CREATE DATABASE api`
    - `GRANT ALL PRIVILEGES ON *.* TO 'apiuser'`

- While containers are up, connect to the `wm_dashboard` database and import any .sql files you may have for the initial table structure and seed data.
- Run `docker-compose exec php php artisan migrate` to add oauth and users tables (plus a couple indices).
- Run `docker-compose exec php php artisan db:seed` to add default user(s) to users table (development/staging only - manually add users in production).
- Run `docker-compose exec php php artisan passport:install` to create clients and their secrets for passport. Be sure to write these down.

## Running Commands
As seen in [Database Setup](#markdown-header-database-setup), `docker-compose exec [container name] [command]` lets us run commands inside containers.

### Single Command Examples
- `docker-compose exec php php artisan make:model User`
- `docker-compose exec php php artisan make:controller UserController --resource`
- `docker-compose exec php php artisan make:resource UserResource`
- `docker-compose exec php php artisan make:resource UserCollection --collection`

### Multiple Command Example
- `docker-compose exec php /bin/sh`
- `php artisan make:model User`
- `exit`

## Making Changes
To change configuration values, look in the `docker-compose.yml` file and change the `php` container's environment variables. These directly correlate to the Lumen environment variables (typically found in the `.env` file).

### [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

### Versioning
One of the processes not mentioned in Gitflow is how to go about deciding version numbers. For this, we use [semantic versioning](https://semver.org/). To link the two processes together, we can think of releases as the first two numbers (major and minor) and hotfixes as the last number (patch). For example, each of the following would be used as a tag when "finishing" a release or hotfix:

- 1.1.1 -> 2.0.0 means this release included breaking changes.
- 1.1.1 -> 1.2.0 means this release included no breaking changes.
- 1.1.1 -> 1.1.2 means this was a hotfix.

#### Major Releases
For new major releases, we should have the ability to support previous versions. Since v2.0.0 would mean breaking changes for multiple clients we need to continue hosting access for v1.x.x so each client can upgrade independently. To be more specific, create a new [support branch](https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/#support-branches) and update the Docker + Nginx configuration in the `develop` branch...should involve a new Dockerfile, configured to pull down code from the new support branch, `docker-compose.yml` can use that to build the image and the port configured so it doesn't overlap, then proxied to `/v1` (Ex. `http://green-api.com/v1`) from the Nginx proxy. We may choose to deprecate and eventually remove the oldest version proxies.

NOTE: Currently, /v1 is included in our routing code, we need to remove this from the code-level and move it to the infrastructure before our first major release.

### Coming From Traditional PHP
A few features of PHP weren't always a part of "the PHP way" of development. In fact, you can be very successful developing with knowledge of only a much smaller subset of the language's syntax. Laravel/Lumen relies on the full range of PHP features, though - especially those related to [OOP](http://php.net/manual/en/language.oop5.php). If you haven't used these as a part of your previous development processes, it could be helpful to start there. To list a few common "gotchas" for those coming from traditional PHP development...

- [namespace/use](http://php.net/manual/en/language.namespaces.rationale.php)
- [autoloading](http://php.net/manual/en/language.oop5.autoload.php) (see the [PSR-4](https://www.php-fig.org/psr/psr-4/) implementation)
- [Composer](https://getcomposer.org/doc/00-intro.md)

...are important to understand together. But also...

- [Magic Methods](http://php.net/manual/en/language.oop5.magic.php)
- [Type Hinting](http://php.net/manual/en/language.oop5.typehinting.php)
- [Traits](http://php.net/manual/en/language.oop5.traits.php)

...are heavily used by different parts of the framework - both internally and in your project code. (When you need more information than the documentation provides, check out the framework's internal code in the `vendor` folder)

Something you won't find in the documentation above is how Traits are used by Eloquent (Laravel/Lumen's ORM) to get around their limitations. In particular, if an `initializeMappedAttributes()` method exists in the `MappedAttributes` trait, it will automatically be detected and run in the model's internal logic. The Laravel/Lumen frameworks also follow [PSR-2](https://www.php-fig.org/psr/psr-2/) coding standards, which we attempt to maintain in this project to keep consistent with documentation and the framework's internal codebase. Finally, be aware of the globally-available [helper functions](https://laravel.com/docs/5.7/helpers). These are generic utility functions like `array_only()`. (We have also added a few of our own in the `app/Helpers` folder)

### Changing Dependencies

When modifying dependencies in the composer file, you can run `composer install` or `composer update` to force the new dependencies to download. But, rather than set up composer on the host machine, you can choose to run it within a one-off docker container like this:

#### Mac/Linux

```bash
docker run --rm --interactive --tty --volume $PWD/images/php/app:/app composer install --ignore-platform-reqs --no-scripts
```

#### Windows

```bat
docker run --rm --interactive --tty --volume %CD%/images/php/app:/app composer install --ignore-platform-reqs --no-scripts
```

(Don't forget to use `--no-dev` in production)

### Architecture
The program flow is generally
[Route](https://laravel.com/docs/5.7/routing) ->
[Controller Method](https://laravel.com/docs/5.7/controllers) ->
return [Resource](https://laravel.com/docs/5.7/eloquent-resources)([Model](https://laravel.com/docs/5.7/eloquent#defining-models)). I've provided links to documentation for each of these parts, but here are some quick explanations:

- **[Route](https://laravel.com/docs/5.7/routing)**: A URL endpoint. These are represented by functions in `routes/web.php` and connect to Controller methods, which handle a Request.
- **[Request*](https://laravel.com/docs/5.7/requests)**: A container that holds and validates the HTTP request data. See files in `app/Http/Requests/`. NOTE: Type-hinting on a Controller method can be a "gotcha", since it automatically runs logic on the Request.
- **[Controller](https://laravel.com/docs/5.7/controllers)**: A collection of methods that use Requests (input), Models, and Resources (output) to return an API response. See files in `app/Http/Controllers/`.
- **[Resource*](https://laravel.com/docs/5.7/eloquent-resources)**: An abstraction over a given Model. Builds the response data for a route. See files in `app/Http/Resources/`.
- **[Model](https://laravel.com/docs/5.7/eloquent#defining-models)**: Represents a business model/resource in our system. See `[ModelName].php` in `app/`. This is where most of our shared logic should be placed. (NOTE: If you find yourself confused, know that Eloquent models operate with a heavy reliance on [magic methods](http://php.net/manual/en/language.oop5.magic.php), which could be throwing you off)

    \* - When building Requests and Resources, please note that this API is intended to follow [JSON:API](https://jsonapi.org/) standards.

#### GET Order Example
##### **routes/web.php**

```php
$router->get('orders/{id}', 'OrderController@show');
```

This funnels HTTP requests like `GET http://localhost:8080/orders/10` to the OrderController::show() method, capturing `id` and passing it as a parameter to the function.


##### **app/Http/Requests/OrderReadRequest.php**

```php
namespace App\Http\Requests;

class OrderReadRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|min:0|exists:wm_orders,id',
        ];
    }
}
```

The `OrderReadRequest` class is simply used to validate the request data before it reaches `OrderController::show()`.


##### **app/Http/Controllers/OrderController.php**

```php
namespace App\Http\Controllers;

use App\Http\Requests\OrderReadRequest;
use App\Http\Resources\OrderResource;
use App\Order;

class OrderController extends Controller
{
    /**
     * Return an OrderResource.
     *
     * @param \App\Http\Requests\OrderReadRequest $request
     * @param string $id
     * @return \App\Http\Resources\OrderResource
     */
    public function show(OrderReadRequest $request, $id)
    {
        // NOTE: Validation happens prior to the first line here

        // Get Order model using the id from the URL
        $order = Order::find($id);

        // Format output using the order model
        return new OrderResource($order);
    }
}
```

From here, we can see the [Eloquent ORM](https://laravel.com/docs/5.7/eloquent) being used to get us an `Order` model. Then, we convert that into a response via `OrderResource`.


##### **app/Http/Resources/OrderResource.php**

```php
namespace App\Http\Resources\Printro;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $this->something is mostly the same as $order->something
        return [
            'type' => 'orders',
            'id' => $this->id,
            'attributes' => [
                'poNumber' => $this->po_number,
                'comments' => $this->comments,
            ],
        ];
    }
}
```

This just builds the response as an array, which later gets converted to JSON.


#### Links
- [Lumen Documentation](https://lumen.laravel.com/docs/5.7)
- [Laravel Documentation](https://laravel.com/docs/5.7)
- [JSON:API Introduction](https://laravel-news.com/json-api-introduction)
- [Laravel API Resources + JSON API Spec (Medium article)](https://medium.com/@samushi/laravel-api-resources-json-api-spec-27df4f3ea514)

### Testing
To run tests, use `docker-compose exec php vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests`

## Additional Notes
This space is reserved for any notable resources or complications/lessons learned encountered during the evolution of this project.

### Docker Notes
This project was originally created using https://github.com/saada/docker-lumen as a base.

### Authentication notes
This application was built as a Lumen project to start, which was lacking an official Passport implementation of its own. So, we used [Dusterio's lumen-passport package](https://github.com/dusterio/lumen-passport) for authentication.

### Route Notes
We are using the `POST` method for requests requiring a lot of input data that would otherwise use `GET`.

### Database Notes
MySQL 8 had a few problems with the default setup. We had to add `--default-authentication-plugin=mysql_native_password` to the `docker-compose.yml` file and had to extract Laravel's config/database.php to add MySQL's "modes"
https://stackoverflow.com/questions/49949526/laravel-mysql-migrate-error
